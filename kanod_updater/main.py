#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import subprocess
import sys
from typing import cast, Any, Dict, Optional  # noqa: H301

import argparse
from kubernetes import client  # type: ignore
from kubernetes import config  # type: ignore
import logging
import os
import yaml

from . import config_util

log = logging.getLogger(__name__)

ARGOCD_ENV_PREFIX = "ARGOCD_ENV_"
RENDER_PATH = "/src/render"

def read_yaml(name: str) -> config_util.YamlDict:
    with open(name, 'r') as fd:
        config = yaml.load(fd, Loader=yaml.Loader)
    return config


def read_yaml_string(spec: str) -> config_util.YamlDict:
    return yaml.load(spec, Loader=yaml.Loader)


def getPoolUserAndNetworkList(cluster_name: str, cdef_namespace: str) -> Dict:
    error_handler = config_util.ErrorHandler()
    config.load_incluster_config()
    api = client.CustomObjectsApi()
    cdef = api.get_namespaced_custom_object(
        group='gitops.kanod.io',
        version="v1alpha1",
        namespace=cdef_namespace,
        plural="clusterdefs",
        name=cluster_name)

    data = {}
    if cdef is None:
        error_handler.error(
            'infra',
            f'Cannot read clusterdef {cluster_name} '
            'in namespace {cdef_namespace}')
        raise config_util.UpdaterException()

    if "poolUserList" in cdef["spec"]:
        data["poolUserList"] = cdef["spec"]["poolUserList"]

    if "networkList" in cdef["spec"]:
        data["networkList"] = cdef["spec"]["networkList"]

    return data


def add_kpt_requirements(config: config_util.YamlDict):
    for k, v in config.items():
        config[k] = {
            'apiVersion': 'v1',
            'kind': v.get('kind', 'kubeadm') if k == 'cluster' else str(k).capitalize(),
            'metadata': {'name': k},
            **v
        }


def replace_kptfile_values(values: config_util.YamlDict):
    for k, v in values.items():
        cmd = subprocess.run(f"sed -i '/{k}: {k.upper()}/c\        {k}: {v}' {RENDER_PATH}/Kptfile",
            shell=True,
            stderr=subprocess.PIPE, encoding='utf-8')
        if cmd.returncode != 0:
            print(
                ('sed failed: %d %s' %
                (cmd.returncode, cmd.stderr)),
                file=sys.stderr)
            exit(1)


def save_config_files(config: config_util.YamlDict):
    for k, v in config.items():
        with open(f'{RENDER_PATH}/{k}.yaml', 'w') as file:
            yaml.dump(v, file)


def update(
    conf_path: str,
    cluster_path: str,
    cluster_name: str,
    brokerdata_path: str,
    cidata_path: str,
    namespace: Optional[str],
    output: Optional[str]
):
    error_handler = config_util.ErrorHandler()
    try:
        config_util.setup_yaml_formatter()
        config = read_yaml(conf_path)
        descr = read_yaml(cluster_path)
        cidata = {}
        if cidata_path != "":
            cidata = cast(Dict[str, str], read_yaml(cidata_path))
        brokerdata = ""
        if brokerdata_path != "":
            brokerdata = cast(Dict[str, Dict[Any, Any]],
                              read_yaml(brokerdata_path))
        if output is not None:
            global_config = cast(
                config_util.YamlDict,
                {
                    'infra': config,
                    'cluster': descr,
                    'cidata': cidata,
                    'broker': brokerdata
                }
            )
            values = cast (
                config_util.YamlDict,
                {
                    'name': cluster_name,
                    'namespace': namespace
                }
            )
            add_kpt_requirements(global_config)
            save_config_files(global_config)
            replace_kptfile_values(values)
            cmd = subprocess.run(f'kpt fn render pipeline --allow-network --truncate-output=false -o unwrap > {output}',
                shell=True,
                stderr=subprocess.PIPE, encoding='utf-8')
            if cmd.returncode != 0:
                print(
                    ('kpt failed: %d %s' %
                    (cmd.returncode, cmd.stderr)),
                    file=sys.stderr)
                exit(1)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)


def update_in_cluster(
    cdef_ns: str,
    conf_ns: str,
    conf_name: str,
    cidata_name: str,
    cluster_path: str,
    cluster_name: str,
    namespace: Optional[str],
):
    error_handler = config_util.ErrorHandler()
    try:
        config_util.setup_yaml_formatter()
        descr = read_yaml(cluster_path)

        config.load_incluster_config()
        corev1 = client.CoreV1Api()
        cm = corev1.read_namespaced_config_map(conf_name, conf_ns)
        if cm is None:
            error_handler.error(
                'infra',
                f'Cannot find infrastructure configmap for {cluster_name}')
            raise config_util.UpdaterException()
        infra_text = cm.data.get('config', None)
        if infra_text is None:
            error_handler.error(
                'infra', 'config is not defined in infra configmap'
                f'for cluster {cluster_name}')
            raise config_util.UpdaterException()

        infra = read_yaml_string(infra_text)
        if cidata_name == "":
            log.info(f'No specific cidata found for {cluster_name}')
            cidata = {}
        else:
            cm_cidata = corev1.read_namespaced_config_map(cidata_name, conf_ns)
            cidata = cm_cidata.data
        broker_data = getPoolUserAndNetworkList(cluster_name, cdef_ns)
        global_config = cast(
            config_util.YamlDict,
            {
                'infra': infra,
                'cluster': descr,
                'cidata': cidata,
                'broker': broker_data
            }
        )
        values = cast (
            config_util.YamlDict,
            {
                'name': cluster_name,
                'namespace': namespace
            }
        )
        add_kpt_requirements(global_config)
        save_config_files(global_config)
        replace_kptfile_values(values)
        cmd = subprocess.run(f'kpt fn render {RENDER_PATH} --allow-network --truncate-output=false -o unwrap',
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, encoding='utf-8')
        if cmd.returncode != 0:
            print(
                ('kpt failed: %d %s' %
                (cmd.returncode, cmd.stderr)),
                file=sys.stderr)
            exit(1)
        print(cmd.stdout)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)


def vault_kustomize():
    os.environ['SSL_CERT_DIR'] = '/etc/ssl/certs:/app/config/tls'
    print(os.environ, file=sys.stderr)
    proc1 = subprocess.Popen(
        ['kustomize', 'build', '.'],
        stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
    proc2 = subprocess.Popen(
        ['argocd-vault-plugin', 'generate', '-'],
        stdin=proc1.stdout, stderr=subprocess.PIPE, encoding='utf-8')
    proc1.stdout.close()
    ret1 = proc1.wait()
    if ret1 != 0:
        errs = proc1.communicate()[1]
        print(
            'kustomize failed: %d %s' % (ret1, errs),
            file=sys.stderr)
    ret2 = proc2.wait()
    if ret2 != 0:
        errs = proc2.communicate()[1]
        print(
            'vault plugin failed: %d %s' % (ret2, errs),
            file=sys.stderr)
        exit(1)
    print('Successful kustomize / vault plugin run', file=sys.stderr)
    exit(0)


def argocd_get(key, default):
    return os.environ.get(
        ARGOCD_ENV_PREFIX + key, os.environ.get(key, default))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config",
        help="Configuration file for infra requirements"
    )
    parser.add_argument(
        "-d", "--discover", action=argparse.BooleanOptionalAction,
        help="Discover mode for argocd plugin"
    )
    parser.add_argument(
        "-k", "--kubernetes", action=argparse.BooleanOptionalAction,
        help="Parse for kubernetes mode from environment variables"
    )
    parser.add_argument(
        "-C", "--configmap",
        help="Configuration configmap name for infra requirements"
    )
    parser.add_argument(
        "-o", "--output",
        help="Output file for cluster-api (YAML)"
    )
    parser.add_argument(
        '-e', '--cdefns',
        help='Specify clusterdef namespace'
    )
    parser.add_argument(
        '-n', '--namespace',
        help='Specify output namespace'
    )
    parser.add_argument(
        "-N", "--cmns",
        help="Configuration configmap namespace for infra requirements"
    )
    parser.add_argument(
        "-m", "--cidata",
        help="Cloud-init metadata path to file or configmap name"
    )
    parser.add_argument(
        "-b", "--brokerdata",
        help="brokerdata path to file or configmap name"
    )
    parser.add_argument(
        "-l", "--clustername", help="cluster name"
    )
    parser.add_argument(
        "-p", "--cluster", help="cluster description"
    )
    args = parser.parse_args()
    if args.discover:
        if argocd_get('PLUGIN_MODE', None) is not None:
            print("OK")
        exit(0)
    elif args.kubernetes:
        mode = argocd_get('PLUGIN_MODE', '')
        if mode == 'vault':
            cmd = subprocess.run(
                ['argocd-vault-plugin', 'generate', './'],
                stderr=subprocess.PIPE, encoding='utf-8')
            if cmd.returncode != 0:
                print(
                    ('vault plugin failed: %d %s' %
                     (cmd.returncode, cmd.stderr)),
                    file=sys.stderr)
                exit(1)
        elif mode == 'vault-kustomize':
            vault_kustomize()
        elif mode == 'updater':
            update_in_cluster(
                cdef_ns=argocd_get('CDEF_NAMESPACE', ''),
                conf_ns=argocd_get('INFRA_NAMESPACE', ''),
                conf_name=argocd_get('INFRA_CONFIGMAP', ''),
                cidata_name=argocd_get('CIDATA_CONFIGMAP', ''),
                cluster_path=argocd_get('CLUSTER_CONFIG_FILE', ''),
                cluster_name=argocd_get('CLUSTER_NAME', ''),
                namespace=argocd_get('TARGET_NAMESPACE', '')
            )
        else:
            if os.path.exists('kustomization.yaml'):
                if os.system('kustomize build --enable-helm .') != 0:
                    exit(1)
            else:
                os.system(
                    "find . \\( -name '*.yaml' -o -name '*.yml' \\) "
                    "-exec cat {} \\; -exec echo '---' \\;")
    elif args.configmap is not None:
        update_in_cluster(
            cdef_ns=args.cdefns, conf_ns=args.cmns, conf_name=args.configmap,
            cidata_name=args.cidata, cluster_path=args.cluster,
            cluster_name=args.clustername, namespace=args.namespace)
    else:
        update(
            conf_path=args.config, cluster_path=args.cluster,
            brokerdata_path=args.brokerdata, cidata_path=args.cidata,
            cluster_name=args.clustername,
            namespace=args.namespace, output=args.output)
