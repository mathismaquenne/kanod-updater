#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import Dict, List, Union   # noqa: H301
import contextlib
import sys
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader   # type: ignore[assignment]


def load(filename):
    '''Load a yaml file from a file descriptor

    :param filename: yaml file
    :return: object corresponding to yaml file
    '''
    try:
        with open(filename, 'r', encoding='utf-8') as fd:
            result = yaml.load(fd, Loader=Loader)
    except yaml.parser.ParserError as e:
        sys.exit(f'Yaml loading exited with error: {e}')
    except FileNotFoundError as e:
        sys.exit(f'Yaml file not found: {e}')
    except OSError as e:
        sys.exit(f'Yaml file cannot be read: {e}')
    return result


class literal(str):
    '''long string class

    A class to wrap long multi-line strings that should be output in yaml
    using the pipe.
    '''

    pass


def literal_presenter(dumper, data):
    '''Function to output literal in pyyaml'''
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')


def setup_yaml_formatter():
    '''Add a formatter for class literal used for long strings'''
    yaml.add_representer(literal, literal_presenter)


def dump(content, filename=None, header=None):
    '''Dumps yaml to a file/string with optional header'''
    if filename is None:
        return yaml.dump(content)
    else:
        try:
            with open(filename, 'w') as fd:
                if header is not None:
                    fd.write(header)
                yaml.dump(content, stream=fd)
        except OSError as e:
            sys.exit(f'Cannot create Yaml file {filename}: {e}')


@contextlib.contextmanager
def extended_open(filename: str, append=False):
    '''Opens either a file or use stdout if input is -

    The goal is to correctly close the output only for a file.
    '''
    fd = (
        sys.stdout if filename == '-'
        else open(filename, 'a' if append else 'w', encoding='utf-8'))
    try:
        yield fd
    finally:
        if fd is not sys.stdout:
            fd.close()


class UpdaterException(Exception):
    '''UpdaterException is the error local to the updater

    It is raised by the error handler during a check when there are errors
    recorded in it. The goal is to abort the computation and report errors
    at the top-level.

    The goal is to defer stopping the computation as much as we can.
    '''
    pass


class ErrorHandler:
    '''Global error handler

    This class defines an error/warning handler that accumulates errors for
    the end-user. It should not be used for internal errors.
    '''
    def __init__(self):
        self.errors = []
        self.warnings = []

    def error(self, kind: str, message: str):
        '''Register an error for reporting'''
        self.errors.append((kind, message))

    def warning(self, kind: str, message: str):
        '''Register a warning for reporting'''
        self.warnings.append((kind, message))

    def abort_on_errors(self):
        '''When this function is called, aborts computations. '''
        if len(self.errors) > 0:
            raise UpdaterException()


'''YamlObj is the type representing an arbitrary json object'''
YamlObj = Union[  # type: ignore[misc]
    str, int, bool, literal,
    List['YamlObj'], 'YamlDict']  # type: ignore[misc]

'''YamlDict represents a parsed json/yaml structure'''
YamlDict = Dict[  # type: ignore[misc]
    str,
    YamlObj
]
