import logging
import os
import sys
import yaml
from typing import cast, List, Optional

project_root = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'src')
sys.path.append(project_root)

import byoh_kubeadm
import byoh_machines
import config_util
import data
import kamaji_kubeadm
import kubeadm
import machines
import rke2
from updater import Cluster
import versions

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_manifests(
    error_handler: config_util.ErrorHandler,
    resource_list: config_util.YamlDict
):
    items = cast(
        List[config_util.YamlDict],
        resource_list.get('items', []))
    
    cluster_config, infra_config, controlplane, workers, cidata = config_util.get_configs(error_handler, items)
    metadata = cast(
        config_util.YamlDict,
        cluster_config.get('metadata', {}))
    cluster_name, namespace = metadata.get('name'), metadata.get('namespace')
    
    cluster = Cluster(
        error_handler,
        cluster_name,
        infra_config,
        cluster_config,
        controlplane,
        workers,
        cidata,
        namespace=namespace
    )

    metadata_name, _ = data.metadata_manifest(cluster, cluster.ippools_bindings)
    cp_spec = cast(
        Optional[config_util.YamlDict],
        cluster.controlplane)
    if not cp_spec:
        raise Exception('cluster must define a control-plane')
    cp_version = versions.controlplane_version(cluster)
    if not cp_version:
        cluster.error_handler.error(
            'validation',
            'Cannot proceed without Kubernetes version for'
            f'control-plane of {cluster.cluster_name}')
        return

    if cluster.controlplane.get("infraProvider", "metal3") == "metal3":
        if cluster.rke2:
            controlplane = rke2.rke2_controlplane_manifest(cluster, cp_version)
        else:
            controlplane = kubeadm.kubeadm_controlplane_manifest(cluster, cp_version)
        cp_machine = machines.machine_template_manifest(
            cluster, None, cp_spec, metadata_name, cp_version)

        items += [controlplane, cp_machine]

    if cluster.controlplane.get("infraProvider", "metal3") == "byoh":
        controlplane = byoh_kubeadm.byoh_kubeadm_controlplane_manifest(
            cluster, cp_version)
        cp_machine = byoh_machines.machine_template_manifest(
            cluster, None, cp_spec, metadata_name, cp_version)
        k8s_installer = byoh_machines.k8s_installer_manifest(
            cluster, None, cp_spec, metadata_name, cp_version)

        items += [controlplane, cp_machine, k8s_installer]

    if cluster.controlplane.get("infraProvider", "metal3") == "kamaji":
        controlplane = kamaji_kubeadm.kamaji_kubeadm_controlplane_manifest(
            cluster, cp_version)

        items += [controlplane]

    return yaml.dump_all(items)

if __name__ == '__main__':
    error_handler = config_util.ErrorHandler()

    stdin = sys.stdin.read()
    resource_list = cast(
        config_util.YamlDict,
        yaml.load(stdin, Loader=yaml.Loader))
    try:
        result = get_manifests(error_handler, resource_list)
        print(result)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)
