import logging
import os
import sys
import yaml
from typing import cast, List

project_root = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'src')
sys.path.append(project_root)

import config_util
import deployment
import machine_healthcheck
import resourceset
from updater import Cluster
import versions

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_manifests(
    error_handler: config_util.ErrorHandler,
    resource_list: config_util.YamlDict
):
    items = cast(
        List[config_util.YamlDict],
        resource_list.get('items', []))
    
    cluster_config, infra_config, controlplane, workers, cidata = config_util.get_configs(error_handler, items)
    metadata = cast(
        config_util.YamlDict,
        cluster_config.get('metadata', {}))
    cluster_name, namespace = metadata.get('name'), metadata.get('namespace')
    
    cluster = Cluster(
        error_handler,
        cluster_name,
        infra_config,
        cluster_config,
        controlplane,
        workers,
        cidata,
        namespace=namespace
    )

    cp_version = versions.controlplane_version(cluster)
    if not cp_version:
        cluster.error_handler.error(
            'validation',
            'Cannot proceed without Kubernetes version for'
            f'control-plane of {cluster.cluster_name}')
        return

    mhc_manifest = machine_healthcheck.worker_manifest(cluster)
    items += [mhc_manifest]

    mhc_manifest = machine_healthcheck.cp_manifest(cluster)
    items += [mhc_manifest]

    if cluster.need_autoscaler:
        items += deployment.autoscaler(cluster)
    items += resourceset.generate_inlined_addons(cluster, cp_version)

    return yaml.dump_all(items)

if __name__ == '__main__':
    error_handler = config_util.ErrorHandler()

    stdin = sys.stdin.read()
    resource_list = cast(
        config_util.YamlDict,
        yaml.load(stdin, Loader=yaml.Loader))
    try:
        result = get_manifests(error_handler, resource_list)
        print(result)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)
