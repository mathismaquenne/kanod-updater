import logging
import os
import sys
import yaml
from typing import cast, List

project_root = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'src')
sys.path.append(project_root)

import baremetalpool
import config_util
from updater import Cluster
import versions

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_manifests(
    error_handler: config_util.ErrorHandler,
    resource_list: config_util.YamlDict
):
    items = cast(
        List[config_util.YamlDict],
        resource_list.get('items', []))

    broker_data = next((item for item in items
        if item.get('kind') == 'Broker'), {})
    if not broker_data:
        error_handler.error(
            'config',
            f'Cannot find the broker.yaml file')
        raise config_util.UpdaterException()

    cluster_config, infra_config, controlplane, workers, cidata = config_util.get_configs(error_handler, items)
    metadata = cast(
        config_util.YamlDict,
        cluster_config.get('metadata', {}))
    cluster_name, namespace = metadata.get('name'), metadata.get('namespace')
    
    cluster = Cluster(
        error_handler,
        cluster_name,
        infra_config,
        cluster_config,
        controlplane,
        workers,
        cidata,
        namespace=namespace
    )

    cp_version = versions.controlplane_version(cluster)
    if not cp_version:
        cluster.error_handler.error(
            'validation',
            'Cannot proceed without Kubernetes version for'
            f'control-plane of {cluster.cluster_name}')
        return

    pool_network_map = baremetalpool.get_pool_network_map(cluster)
    for bmpool in cast(List[config_util.YamlDict], cluster.baremetalpools):
        bm_resource = baremetalpool.baremetalpool_manifest(
            cluster,
            bmpool,
            broker_data["poolUserList"],
            pool_network_map,
            cp_version)
        items += bm_resource

    return yaml.dump_all(items)

if __name__ == '__main__':
    error_handler = config_util.ErrorHandler()

    stdin = sys.stdin.read()
    resource_list = cast(
        config_util.YamlDict,
        yaml.load(stdin, Loader=yaml.Loader))
    try:
        result = get_manifests(error_handler, resource_list)
        print(result)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)
