import logging
import os
import sys
import yaml
from typing import cast, List

project_root = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'src')
sys.path.append(project_root)

import config_util
from updater import specialize
import validator

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def init_cluster(
    error_handler: config_util.ErrorHandler,
    resource_list: config_util.YamlDict
):
    cm = cast(
        config_util.YamlDict,
        resource_list.get('functionConfig', {}).get('data'))
    if not cm:
        error_handler.error(
            'config',
            f'Cannot find configmap')
        raise config_util.UpdaterException()

    name, namespace = cm.get('name'), cm.get('namespace')
    if not name or not namespace:
        error_handler.error(
            'config',
            f'Cannot find required keys in configmap')
        raise config_util.UpdaterException()

    items = cast(
        List[config_util.YamlDict],
        resource_list.get('items', []))

    items_to_remove = ['Kptfile']
    items = [item for item in items if item.get('kind') not in items_to_remove]

    cluster_config = next((item for item in items
        if item.get('kind') in ['kubeadm', 'rke2']), None)
    if not cluster_config:
        error_handler.error(
            'config',
            f'Cannot find cluster.yaml file')
        raise config_util.UpdaterException()
    
    infra_config = next((item for item in items
        if item.get('kind') == 'Infra'), None)
    if not infra_config:
        error_handler.error(
            'config',
            f'Cannot find infra.yaml file')
        raise config_util.UpdaterException()
    
    validator.validate_infra(error_handler, infra_config)
    validator.validate_cluster(error_handler, cluster_config)

    error_handler.abort_on_errors()

    metadata: config_util.YamlDict = {
        'name': name,
        'namespace': namespace
    }
    cluster_config['metadata'] = metadata

    cp_flavours = cast(
        List[config_util.YamlDict],
        infra_config.get('controlPlaneFlavours'))
    cluster_cp = cast(
        config_util.YamlDict,
        cluster_config.get('controlPlane'))
    full_controlplane = specialize(error_handler, cluster_cp, cp_flavours)
    full_controlplane = cast(
        config_util.YamlDict,
        {
            'apiVersion': 'v1',
            'kind': 'Merge',
            'metadata': {'name': 'controlplane'},
            **full_controlplane
        }
    )

    worker_flavours = cast(
        List[config_util.YamlDict],
        infra_config.get('workerFlavours', []))
    server_flavours = cast(
            List[config_util.YamlDict],
            infra_config.get('serverFlavours', []))
    cluster_workers = cast(
        List[config_util.YamlDict],
        cluster_config.get('workers', []))
    workers = [
        specialize(error_handler, worker, worker_flavours)
        for worker in cluster_workers
    ]
    workers = cast(
        config_util.YamlDict,
        {
            'apiVersion': 'v1',
            'kind': 'Merge',
            'metadata': {'name': 'workers'},
            'workers': workers
        }
    )

    items.extend((full_controlplane, workers))

    return yaml.dump_all(items)

if __name__ == '__main__':
    error_handler = config_util.ErrorHandler()

    stdin = sys.stdin.read()
    resource_list = cast(
        config_util.YamlDict,
        yaml.load(stdin, Loader=yaml.Loader))
    try:
        result = init_cluster(error_handler, resource_list)
        print(result)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)
