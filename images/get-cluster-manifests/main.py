import logging
import os
import sys
import yaml
from typing import cast, List

project_root = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'src')
sys.path.append(project_root)

import byoh_cluster as byoh_cluster_manifest
import config_util
import cluster as cluster_manifest
from updater import Cluster

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_manifests(
    error_handler: config_util.ErrorHandler,
    resource_list: config_util.YamlDict
):
    items = cast(
        List[config_util.YamlDict],
        resource_list.get('items', []))
    
    cluster_config, infra_config, controlplane, workers, cidata = config_util.get_configs(error_handler, items)
    metadata = cast(
        config_util.YamlDict,
        cluster_config.get('metadata', {}))
    cluster_name, namespace = metadata.get('name'), metadata.get('namespace')
    
    cluster = Cluster(
        error_handler,
        cluster_name,
        infra_config,
        cluster_config,
        controlplane,
        workers,
        cidata,
        namespace=namespace
    )

    cluster_spec = cluster_manifest.cluster_manifest(cluster)
    items.append(cluster_spec)

    if ("metal3" in cluster.infraProviders or
       "kamaji" in cluster.infraProviders):
        log.info('Metal3 cluster generation')
        metal3_cluster = cluster_manifest.metal3_cluster_manifest(cluster)
        items.append(metal3_cluster)
    if "byoh" in cluster.infraProviders:
        log.info('BYOH cluster generation')
        byoh_cluster = byoh_cluster_manifest.byoh_cluster_manifest(cluster)
        items.append(byoh_cluster)

    return yaml.dump_all(items)

if __name__ == '__main__':
    error_handler = config_util.ErrorHandler()

    stdin = sys.stdin.read()
    resource_list = cast(
        config_util.YamlDict,
        yaml.load(stdin, Loader=yaml.Loader))
    try:
        result = get_manifests(error_handler, resource_list)
        print(result)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)
