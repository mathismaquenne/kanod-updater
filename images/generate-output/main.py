import logging
import os
import sys
from typing import cast, List

from ruamel.yaml import YAML
from ruamel.yaml.scalarstring import LiteralScalarString

yaml = YAML()
yaml.preserve_quotes = True

project_root = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'src')
sys.path.append(project_root)

import config_util

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def convert_to_literal_block(value):
    return LiteralScalarString(value)

def recursive_walk(data):
    if isinstance(data, dict):
        for k, v in data.items():
            if k == 'content' or k == 'res':
                data[k] = convert_to_literal_block(v)
            else:
                recursive_walk(v)
    elif isinstance(data, list):
        for i in range(len(data)):
            recursive_walk(data[i])

def generate_output(
    error_handler: config_util.ErrorHandler,
    resource_list: config_util.YamlDict
):
    items = cast(
        List[config_util.YamlDict],
        resource_list.get('items', []))
    
    items_to_remove = ['Broker', 'Cidata', 'Infra', 'kubeadm', 'Merge', 'rke2']
    items = [item for item in items if item.get('kind') not in items_to_remove]

    for item in items:
        recursive_walk(item)

    yaml.dump_all(items, sys.stdout)

if __name__ == '__main__':
    error_handler = config_util.ErrorHandler()

    stdin = sys.stdin.read()
    resource_list = cast(
        config_util.YamlDict,
        yaml.load(stdin))
    try:
        generate_output(error_handler, resource_list)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)
