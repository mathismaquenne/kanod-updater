import logging
import os
import sys
import yaml
from typing import cast, List

project_root = os.path.join(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'src')
sys.path.append(project_root)

import byoh_deployment
import config_util
import data
import deployment
from updater import Cluster

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_manifests(
    error_handler: config_util.ErrorHandler,
    resource_list: config_util.YamlDict
):
    items = cast(
        List[config_util.YamlDict],
        resource_list.get('items', []))
    
    cluster_config, infra_config, controlplane, workers, cidata = config_util.get_configs(error_handler, items)
    metadata = cast(
        config_util.YamlDict,
        cluster_config.get('metadata', {}))
    cluster_name, namespace = metadata.get('name'), metadata.get('namespace')
    
    cluster = Cluster(
        error_handler,
        cluster_name,
        infra_config,
        cluster_config,
        controlplane,
        workers,
        cidata,
        namespace=namespace
    )

    metadata_name, _ = data.metadata_manifest(cluster, cluster.ippools_bindings)
    for md in cluster.workers:
        if md.get("infraProvider", "metal3") == "metal3":
            resource_deployment = deployment.full_deployment(
                cluster, md, metadata_name)
            items += resource_deployment

        if md.get("infraProvider", "metal3") == "byoh":
            manifests = byoh_deployment.full_deployment(
                cluster, md, metadata_name)

            items += manifests

    return yaml.dump_all(items)

if __name__ == '__main__':
    error_handler = config_util.ErrorHandler()

    stdin = sys.stdin.read()
    resource_list = cast(
        config_util.YamlDict,
        yaml.load(stdin, Loader=yaml.Loader))
    try:
        result = get_manifests(error_handler, resource_list)
        print(result)
    except config_util.UpdaterException:
        for kind, msg in error_handler.errors:
            log.error(f'- {kind}: {msg}')
        exit(1)
