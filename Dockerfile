#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
ARG KUBECTL_VERSION=1.29.1
ARG YQ_VERSION=4.40.5
ARG VAULT_PLUGIN_VERSION=1.16.1
ARG HELM_VERSION=v3.12.3
FROM curlimages/curl:7.78.0 as builder
ARG KUBECTL_VERSION
ARG YQ_VERSION
ARG VAULT_PLUGIN_VERSION
ARG HELM_VERSION
RUN curl -L -o /tmp/kpt https://github.com/mathismqn/kpt/releases/download/v1.0.0-beta.53/kpt_linux_amd64
RUN curl -L -o /tmp/kustomize https://github.com/mathismqn/kustomize/releases/download/kustomize/kustomize_linux_amd64
RUN curl -L -o /tmp/kubectl https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl
RUN curl -L -o /tmp/yq https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64
RUN curl -L -o /tmp/argocd-vault-plugin https://github.com/argoproj-labs/argocd-vault-plugin/releases/download/v${VAULT_PLUGIN_VERSION}/argocd-vault-plugin_${VAULT_PLUGIN_VERSION}_linux_amd64
RUN curl -sLf --retry 3 https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz | tar -C /tmp -zxvf - linux-amd64/helm

USER root
RUN chmod +x /tmp/kpt
RUN chmod +x /tmp/kustomize
RUN chmod +x /tmp/kubectl
RUN chmod +x /tmp/yq
RUN chmod +x /tmp/argocd-vault-plugin

FROM python:3.9-slim
USER root
RUN mkdir /vault-ca
RUN mkdir -p /home/argocd/cmp-server/config

COPY plugin.yaml /home/argocd/cmp-server/config/plugin.yaml
COPY --from=builder /tmp/kpt /usr/local/bin
COPY --from=builder /tmp/kubectl /usr/local/bin
COPY --from=builder /tmp/yq /usr/local/bin
COPY --from=builder /tmp/argocd-vault-plugin /usr/local/bin
COPY --from=builder /tmp/kustomize /usr/local/bin
COPY --from=builder /tmp/linux-amd64/helm /usr/local/bin
COPY kanod_updater /src/kanod_updater
COPY render /src/render
COPY setup.py setup.cfg MANIFEST.in /src/
RUN chown -R 999 /home/argocd
RUN chown -R 999 /src/render

RUN apt-get update || true && apt-get install -y python3-venv jq git
RUN python3 -m venv /install

WORKDIR /src
RUN /install/bin/pip install .
USER 999
