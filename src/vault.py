#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import Optional, Union, Tuple  # noqa: H301

import base64
import hashlib
import json
import logging
import os
import requests
import time


import config_util

TOKEN_PATH = '/var/run/secrets/kubernetes.io/serviceaccount/token'

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def json_hash(config):
    str = json.dumps(config, sort_keys=True, separators=(',', ':'))
    d = hashlib.sha1()
    d.update(str.encode('utf-8'))
    return d.hexdigest()


def get_vault_verify() -> Union[str, bool]:
    vault_ca_file = os.environ.get('VAULT_CACERT', None)
    if vault_ca_file is None:
        log.info('No CA found for Vault')
        return True
    else:
        log.info('CA configured for Vault')
        return vault_ca_file


class VaultHandler:
    '''Handles the interaction between Vault and the updater.'''
    def __init__(
        self,
        cluster_name: str,
        error_handler: config_util.ErrorHandler,
        vault_config
    ):
        '''VaultHandler constructor

        :param cluster_name: name of cluster to be used as key for Vault paths
        '''
        self.vault_token: Optional[str] = None
        self.error_handler = error_handler
        self.cluster_name = cluster_name
        self.vault_config = vault_config
        self.vault_url = os.environ.get('VAULT_ADDR', None)
        self.vault_verify = get_vault_verify()

    def use_tpm(self):
        '''tests if TPM is used for authentication in the cluster'''
        return self.vault_config.get('tpm', False)

    def inject(self, name: str, config):
        '''Inject a secret-id in the declarative cloud-init configuration

        :param name: name of structure (control-plane or deployement).
            Used as key for vault paths.
        :param config: the current config (json) that will be amended for
            vault.
        '''
        if self.vault_url is None:
            return
        vault_role = self.cluster_name
        log.info('Trying to configure vault tokens')
        client_vault = config.setdefault('vault', {})
        use_tpm = self.vault_config.get('tpm', False)
        if use_tpm:
            client_vault['tpm_name'] = '{{ ds.meta_data.name }}'
            client_vault['role_id'] = '{{ ds.meta_data.tpm_role_id }}'
            client_vault['tpm_auth'] = os.environ.get('TPM_AUTH_URL')
            ca_b64 = os.environ.get('TPM_AUTH_CA_B64')
            if ca_b64 is not None:
                client_vault['tpm_auth_ca'] = (
                    base64.b64decode(ca_b64).decode('ascii'))
            limit = ''
        else:
            vault_role_id = self.vault_config.get('roleId', None)
            if vault_role_id is None:
                log.info('No vault usage configured')
                return
            hash = json_hash(config)
            id = f'{self.cluster_name}-{name}'
            rebuild = self.vault_config.get('renew', False)
            self.vault_token = self.get_token()
            if rebuild:
                secret, limit = self.generate_secret_id(id, vault_role, hash)
            else:
                secret, limit = self.check_secret_validity(id, hash)
                if limit == '':
                    secret, limit = self.generate_secret_id(
                        id, vault_role, hash)
            client_vault['secret_id'] = secret
            client_vault['role_id'] = vault_role_id
        client_vault['url'] = self.vault_url
        client_vault['role'] = vault_role
        vault_ca_file = os.environ.get('VAULT_CACERT', None)
        if vault_ca_file is not None:
            with open(vault_ca_file, 'r', encoding='utf-8') as fd:
                vault_ca = fd.read()
            client_vault['ca'] = vault_ca
        else:
            log.warning('No CA injected for Vault')
        return limit

    def get_token(self) -> Optional[str]:
        '''Gets a vault token to access the vault.'''
        vault_role = os.environ.get('AVP_K8S_ROLE', None)
        vault_k8s_mount = os.environ.get('AVP_K8S_MOUNT_PATH', None)
        if self.vault_url is None:
            log.warning('Need a path to Vault')
            return None
        if vault_k8s_mount is None:
            log.warning('Need a k8s Vault role to talk to Vault')
            return None
        if vault_role is None:
            log.warning('Need a k8s Vault role to talk to Vault')
            return None
        with open(TOKEN_PATH, 'r') as fd:
            jwt = fd.read()
        req = requests.post(
            f'{self.vault_url}/v1/{vault_k8s_mount}/login',
            json={'jwt': jwt, 'role': vault_role},
            verify=self.vault_verify
        )
        if req.status_code == 200:
            vault_token = req.json().get('auth', {}).get('client_token', None)
            if vault_token is None:
                self.error_handler.error('vault', 'No token for updater')
                return None
            return vault_token
        else:
            self.error_handler.error(
                'vault',
                f'Cannot proceed without token ({req.status_code}):'
                f' {req.content.decode("utf-8")}')
            return None

    def check_secret_validity(self, id: str, hash: str) -> Tuple[str, str]:
        '''This function checks if the token is still valid

        When the answer is positive, it gives back the validity as a string.
        Otherwise it gives back an empty string.

        The hash argument is used to check if the current config is similar
        to the one used at the time the secret-id was generated. If there is
        a change we force the token renewal as we will get a new one with
        extended life without forcing an additional deployment refresh.
        '''
        req = requests.get(
            f'{self.vault_url}/v1/secret/data/updater/limit/{id}',
            headers={'X-Vault-Token': self.vault_token or ''},
            verify=self.vault_verify
        )
        if req.status_code == 200:
            data = req.json().get('data', {})
            res = data.get('limit', None)
            secret_id = data.get('secret-id', None)
            old_hash = data.get('hash', None)
            if res is not None:
                limit = int(res)
                if limit > time.time() and hash == old_hash:
                    return secret_id, res
            return '', ''
        else:
            return '', ''

    def generate_secret_id(self,
                           id: str,
                           vault_role: str, hash: str) -> Tuple[str, str]:
        vault_token = self.vault_token
        if vault_token is None:
            self.error_handler.error('Vault', 'Vault token is not defined')
            return '', ''
        req = requests.post(
            f'{self.vault_url}/v1/auth/approle/role/{vault_role}/secret-id',
            headers={'X-Vault-Token': vault_token},
            verify=self.vault_verify
        )
        if req.status_code != 200:
            self.error_handler.error(
                'Vault',
                f'Cannot get token for role {vault_role} ({req.status_code}):'
                f' {req.content.decode("utf-8")}')
            return '', ''
        try:
            data = req.json().get('data', {})
            secret_id = data.get('secret_id', None)
            ttl = int(data.get('secret_id_ttl', '0'))
        except Exception:
            self.error_handler.error(
                'vault',
                f'Cannot decode secret-id for {vault_role}:'
                f' {req.content.decode("utf-8")}')
            return '', ''
        if secret_id is None:
            self.error_handler.error('vault', f'No secret-id for {vault_role}')
            return '', ''
        limit = str(int(time.time() + ttl / 2))
        # req = requests.post(
        #     f'{self.vault_url}/v1/kv/data/argocd/ids/{id}',
        #     json={"data": {"secret-id": secret_id}},
        #     headers={'X-Vault-Token': vault_token},
        #     verify=self.vault_verify
        # )
        req = requests.post(
            f'{self.vault_url}/v1/secret/data/updater/limit/{id}',
            json={"data": {'secret-id': secret_id,
                           'limit': limit,
                           'hash': hash}},
            headers={'X-Vault-Token': vault_token},
            verify=self.vault_verify
        )
        return secret_id, limit

    def get_maven_ca(self) -> Optional[str]:
        '''Return the CA associated to certificates generated by Vault CA.

        This function query the ca_chain from Vault:
        * This may not be the CA for Vault site even if it is in Kanod
          in a bottle
        * We want the real CA, not the intermediate one handled by Vault.
        '''

        req = requests.get(
            f'{self.vault_url}/v1/pki/ca_chain',
            verify=self.vault_verify
        )
        if req.status_code == 200:
            return req.content.decode('utf-8')
        return None
