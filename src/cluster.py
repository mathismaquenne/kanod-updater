#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, Optional  # noqa: H301

import config_util
import updater


def cluster_manifest(cluster: updater.Cluster) -> config_util.YamlDict:
    '''Cluster-API manifest of the cluster'''

    networking = cast(
        config_util.YamlDict,
        cluster.k8sconfig.get('network', {}))
    cidr_services = networking.get('services', ['10.96.0.0/12'])
    cidr_pods = networking.get('pods', ['192.168.192.0/18'])
    service_domain = networking.get('domain', 'cluster-services.local')

    if cluster.controlplane.get("infraProvider", "metal3") == "byoh":
        infra_ref_kind = 'ByoCluster'
    else:
        infra_ref_kind = 'Metal3Cluster'

    if cluster.rke2:
        cp_ref_kind = 'RKE2ControlPlane'
        cp_ref_api_version = updater.RKE2_CP_GROUP
    elif cluster.controlplane.get("infraProvider", "metal3") == "kamaji":
        cp_ref_kind = 'KamajiControlPlane'
        cp_ref_api_version = updater.CP_GROUP
    else:
        cp_ref_kind = 'KubeadmControlPlane'
        cp_ref_api_version = updater.CP_GROUP

    spec: Dict[str, config_util.YamlObj] = {
        'clusterNetwork': {
            'services': {
                'cidrBlocks': cidr_services
            },
            'pods': {
                'cidrBlocks': cidr_pods
            },
            'serviceDomain': service_domain
        },
        'infrastructureRef': {
            'apiVersion': updater.CAPM3_GROUP,
            'kind': infra_ref_kind,
            'name': cluster.cluster_name
        },
        'controlPlaneRef': {
            'apiVersion': cp_ref_api_version,
            'kind': cp_ref_kind,
            'name': f'controlplane-{cluster.cluster_name}'
        }
    }

    labels = {'kanod.io/cluster_name': cluster.cluster_name}
    if cluster.controlplane.get("infraProvider", "metal3") == "byoh":
        labels.update({'cni': f'{cluster.cluster_name}-crs-0', 'crs': 'true'})
    annotations = {'kanod.io/use-host': 'true'} if cluster.hybrid else None
    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPI_GROUP,
        'kind': 'Cluster',
        'metadata': cluster.metadata(
            cluster.cluster_name,
            labels, annotations),
        'spec': spec
    }
    return manifest


def metal3_cluster_manifest(cluster: updater.Cluster) -> config_util.YamlDict:
    spec: Dict[str, config_util.YamlObj] = {
        'controlPlaneEndpoint': {
            'host': cluster.endpoint_host,
            'port': cluster.endpoint_port
        },
        'noCloudProvider': False
    }
    bm_config = cast(
        config_util.YamlDict,
        cluster.infra_config.get('baremetal', {}))
    prefixes = cast(
        Optional[str], bm_config.get('dynamicLabelsPrefix', None))
    if prefixes is not None:
        metal3cluster_annots: Optional[Dict[str, str]] = {
            'metal3.io/metal3-label-sync-prefixes': prefixes
        }
    else:
        metal3cluster_annots = None
    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPM3_GROUP,
        'kind': 'Metal3Cluster',
        'metadata': cluster.metadata(
            cluster.cluster_name, annotations=metal3cluster_annots),
        'spec': spec
    }
    return manifest
