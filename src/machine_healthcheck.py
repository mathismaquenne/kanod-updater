#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import config_util
import updater


def getUnhealthyConditions() -> config_util.YamlObj:
    conditions: config_util.YamlObj = [
        {'type': 'Ready', 'status': 'False', 'timeout': '600s'},
        {'type': 'Ready', 'status': 'Unknown', 'timeout': '600s'},
        {'type': 'NodeHealthy', 'status': 'False', 'timeout': '600s'},
        {'type': 'NodeHealthy', 'status': 'Unknown', 'timeout': '600s'},
        ]
    return conditions


def cp_manifest(
    cluster: updater.Cluster
) -> config_util.YamlDict:
    spec: config_util.YamlDict = {
        'clusterName': cluster.cluster_name,
        'selector': {
            'matchLabels': {
                'cluster.x-k8s.io/control-plane': ''
            }
        },
        'maxUnhealthy': '100%',
        'nodeStartupTimeout': '30m',
        'unhealthyConditions': getUnhealthyConditions()
    }

    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPI_GROUP,
        'kind': 'MachineHealthCheck',
        'metadata': cluster.metadata(f'mhc-kcp-{cluster.cluster_name}'),
        'spec': spec
    }
    return manifest


def worker_manifest(
    cluster: updater.Cluster
) -> config_util.YamlDict:
    spec: config_util.YamlDict = {
        'clusterName': cluster.cluster_name,
        'selector': {
            'matchLabels': {
                'nodepool': 'standard-nodepool'
            }
        },
        'maxUnhealthy': '100%',
        'nodeStartupTimeout': '30m',
        'unhealthyConditions': getUnhealthyConditions()
    }

    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPI_GROUP,
        'kind': 'MachineHealthCheck',
        'metadata': cluster.metadata(f'mhc-worker-{cluster.cluster_name}'),
        'spec': spec
    }
    return manifest
