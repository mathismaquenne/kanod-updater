#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Optional, Tuple  # noqa: H301

import config_util
import updater


def ippool_manifest(
    name: str,
    cluster: updater.Cluster,
    ippool: config_util.YamlDict
) -> config_util.YamlDict:

    '''Builds the ipam/ippool manifest

    '''

    def repl(s):
        if not isinstance(s, str):
            return s
        return updater.cidata_replacer(cluster.cidata, s)

    ippool_start = cast(Optional[str], ippool.get('start', None))
    ippool_end = cast(Optional[str], ippool.get('end', None))
    ippool_prefix = cast(Optional[str], ippool.get('prefix', None))
    ippool_gateway = cast(Optional[str], ippool.get('gateway', None))
    ippool_dns = cast(Optional[List[str]], ippool.get('dns', None))
    preAllocations = cast(
        Optional[Dict[str, str]], ippool.get('preAllocations', None))

    if ippool_start is None or ippool_end is None or ippool_prefix is None:
        # We need to escape but it is an internal error, validation should have
        # caught the problem.
        raise ValueError
    pool: List[config_util.YamlObj] = [{
        'start': repl(ippool_start),
        'end': repl(ippool_end),
    }]

    spec: config_util.YamlDict = {
        'clusterName': cluster.cluster_name,
        'namePrefix': f'ippool-{cluster.cluster_name}',
        'pools': pool,
        'prefix': int(repl(ippool_prefix)),
    }
    if ippool_gateway is not None:
        spec['gateway'] = repl(ippool_gateway)
    if ippool_dns is not None:
        spec['dnsServers'] = [
            cast(config_util.YamlObj, repl(d)) for d in ippool_dns]
    if preAllocations is not None:
        spec['preAllocations'] = {
            f'{name}-{k}': repl(v)
            for (k, v) in preAllocations.items()
        }
    manifest: config_util.YamlDict = {
        'apiVersion': updater.IPAM_GROUP,
        'kind': 'IPPool',
        'metadata': cluster.metadata(name),
        'spec': spec
    }
    return manifest


def ippools_manifests(
    cluster: updater.Cluster,
) -> Tuple[Dict[str, str], List[config_util.YamlDict]]:
    ipam_networks: Dict[str, str] = {}
    manifests: List[config_util.YamlDict] = []
    for ippool_def in cluster.ippools:
        name = cast(Optional[str], ippool_def.get('name', None))
        if name is None:
            continue
        if 'network' in ippool_def:
            network = cast(str, ippool_def['network'])
            ipam_networks[network] = name
        else:
            manifest = ippool_manifest(name, cluster, ippool_def)
            manifests.append(manifest)
    return ipam_networks, manifests
