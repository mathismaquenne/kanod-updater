#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Tuple  # noqa: H301

import yaml

import config_util
import updater


def metadata_manifest(
    cluster: updater.Cluster,
    map_key_pools: Dict[str, str],
) -> Tuple[str, config_util.YamlDict]:

    '''Builds the metal3DataTemplate

    This resource brings together various information to provide cloud-init
    meta-data to the nodes. This is used as an information channel between
    baremetal hosts, machines, ipam, updater configmaps and the servers.
    '''
    cluster.cidata.pop('apiVersion')
    cluster.cidata.pop('kind')
    cluster.cidata.pop('metadata')

    baremetal = cast(
        config_util.YamlDict, cluster.infra_config.get('baremetal', {}))
    labels = cast(List[str], baremetal.get('labels', []))
    if cluster.vault.use_tpm():
        labels.append('tpm-role-id')
    annotations = cast(List[str], baremetal.get('annotations', []))
    from_labels: List[config_util.YamlObj] = [
        {
            'key': key.replace('-', '_'), 'object': 'baremetalhost',
            'label': f'kanod.io/{key}'
        }
        for key in labels
    ]
    from_annotations: List[config_util.YamlObj] = [
        {
            'key': key.replace('-', '_'), 'object': 'baremetalhost',
            'annotation': f'kanod.io/{key}'
        }
        for key in annotations
    ]
    string_specs1: List[config_util.YamlObj] = [
        {'key': 'endpoint_host', 'value': cluster.endpoint_host},
        {'key': 'endpoint_port', 'value': str(cluster.endpoint_port)}
    ]
    string_specs2: List[config_util.YamlObj] = [
        {'key': key, 'value': value} for (key, value) in cluster.cidata.items()
    ]
    string_specs = string_specs1 + string_specs2
    meta_data: config_util.YamlDict = {
        'strings': string_specs,
    }
    spec: config_util.YamlDict = {
        'clusterName': cluster.cluster_name,
        'metaData': meta_data
    }

    if len(from_labels) > 0:
        meta_data['fromLabels'] = from_labels
    if len(from_annotations) > 0:
        meta_data['fromAnnotations'] = from_annotations

    if (len(map_key_pools)) > 0:
        for (category, pre) in [
            ('ipAddressesFromIPPool', 'ip'),
            ('prefixesFromIPPool', 'prefix'),
            ('gatewaysFromIPPool', 'gateway'),
            ('dnsServersFromIPPool', 'dns')
        ]:
            meta_data[category] = [
                {'key': f'{pre}_{key}', 'name': ippool_name,
                 'apiGroup': 'ipam.metal3.io', 'kind': 'IPPool'}
                for key, ippool_name in map_key_pools.items()
            ]

    uid = updater.build_uid(yaml.dump(spec))
    name = f'{cluster.cluster_name}-metadata-{uid}'
    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPM3_GROUP,
        'kind': 'Metal3DataTemplate',
        'metadata': cluster.metadata(name),
        'spec': spec
    }
    return (name, manifest)
