#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast  # noqa: F401,H301

import logging

import config_util
import updater

log = logging.getLogger(__name__)


def kamaji_kubeadm_controlplane_manifest(
    cluster: updater.Cluster, version: str
) -> config_util.YamlDict:
    cp_spec = cluster.controlplane
    # inherited during merge from customer spec
    replicas = cast(int, cp_spec.get('replicas', 1))
    spec: config_util.YamlDict = {
        'replicas': replicas,
        'version': version[1:] if version[0] == 'v' else version,
        'deployment': {},
        'dataStoreName': 'default',
        'kubelet': {
            'cgroupfs': 'systemd',
            'preferredAddressTypes': ['ExternalIP', 'InternalIP', 'Hostname']
        },
        'addons': {
            'coreDNS': {},
            'kubeProxy': {}
        },
        'network': {
            'serviceType': 'LoadBalancer',
            'serviceAddress': cluster.endpoint_host
        }
    }
    manifest: config_util.YamlDict = {
        'kind': 'KamajiControlPlane',
        'apiVersion': updater.KAMAJI_CP_GROUP,
        'metadata': cluster.metadata(f'controlplane-{cluster.cluster_name}'),
        'spec': spec
    }
    return manifest


def kubeadmconfig_manifest(
    cluster: updater.Cluster, name: str
) -> config_util.YamlDict:
    manifest: config_util.YamlDict = {
        'kind': 'KubeadmConfigTemplate',
        'apiVersion': updater.BOOTSTRAP_GROUP,
        'metadata': cluster.metadata(f'md-{cluster.cluster_name}-{name}'),
        'spec': {
            'template': {
                'spec': {}
            }
        }
    }
    return manifest
