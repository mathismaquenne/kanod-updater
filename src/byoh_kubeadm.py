#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Optional  # noqa: F401,H301

import logging
import os
import yaml

import config_util
import updater

log = logging.getLogger(__name__)


def enhance_config(
    cluster: updater.Cluster, config: config_util.YamlDict,
) -> config_util.YamlDict:
    '''Add a few fields to the config of servers'''
    return {
        **config,
        'nexus': cluster.nexus,
        'name': '{{ ds.meta_data.name }}'
    }


def byoh_kubeadm_controlplane_manifest(
    cluster: updater.Cluster, version: str
) -> config_util.YamlDict:
    cp_spec = cluster.controlplane
    # inherited during merge from customer spec
    replicas = cast(int, cp_spec.get('replicas', 1))
    server_config = cast(config_util.YamlDict,
                         cp_spec.get('serverConfig', {}))
    config = enhance_config(cluster, server_config)
    cluster.vault.inject('cp', config)
    image = byoh_machines.image_path(cluster, config, version)
    validator.validate_node(
        cluster.error_handler, image, config, cluster.verify)
    selector = cast(
        config_util.YamlDict, cp_spec.get('hostSelector', {}))
    uid = updater.build_uid(f'{image}{yaml.dump(selector, sort_keys=True)}')
    key = f'controlplane-{cluster.cluster_name}'
    extra_args = cast(Dict[str, str], cp_spec.get('kubeletExtraArgs', {}))
    pool = cp_spec.get('pool', None)
    if pool is not None:
        labels = {'kanod.io/baremetalpool': pool}
    else:
        labels = None

    cluster_namespace = cluster.metadata(
        f'controlplane-{cluster.cluster_name}')["namespace"]
    spec: config_util.YamlDict = {
        'replicas': replicas,
        'version': version,
        'machineTemplate': {
            'infrastructureRef': {
                'kind': 'ByoMachineTemplate',
                'apiVersion': updater.CAPM3_GROUP,
                'name': f'controlplane-{cluster.cluster_name}-{uid}',
                'namespace': f'{cluster_namespace}'
                },
        },
        'kubeadmConfigSpec': byoh_kubeadm_config_spec(
            cluster, config, cluster.k8sconfig, key, extra_args)
    }
    manifest: config_util.YamlDict = {
        'kind': 'KubeadmControlPlane',
        'apiVersion': updater.CP_GROUP,
        'metadata': cluster.metadata(f'controlplane-{cluster.cluster_name}',
                                     labels=labels),
        'spec': spec
    }
    return manifest


def byoh_kubeadm_config_spec(
    cluster: updater.Cluster,
    config: config_util.YamlDict,
    controlplane: Optional[config_util.YamlDict],
    key: str,
    kubelet_extra_args: Dict[str, str]
) -> config_util.YamlDict:

    kube_vip_version = os.environ.get('KUBE_VIP_VERSION', 'v0.5.0')
    kube_vip_image = f'ghcr.io/kube-vip/kube-vip:{kube_vip_version}'

    content: config_util.YamlDict = {
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': 'kube-vip',
            'namespace': 'kube-system'
        },
        'spec': {
            'containers': [
                {
                    'args': ['manager'],
                    'env': [
                        {'name': 'cp_enable', 'value': "true"},
                        {'name': 'vip_arp', 'value': "true"},
                        {'name': 'vip_leaderelection', 'value': "true"},
                        {'name': 'vip_address',
                         'value': cluster.endpoint_host},
                        {'name': 'vip_interface',
                         'value': '{{ .DefaultNetworkInterfaceName }}'},
                        {'name': 'vip_leaseduration', 'value': "15"},
                        {'name': 'vip_renewdeadline', 'value': "10"},
                        {'name': 'vip_retryperiod', 'value': "2"}
                    ],
                    'image': kube_vip_image,
                    'imagePullPolicy': 'IfNotPresent',
                    'name': 'kube-vip',
                    'resources': {},
                    'securityContext': {
                        'capabilities': {
                            'add': ['NET_ADMIN', 'NET_RAW']
                        }
                    },
                    'volumeMounts': [
                        {'mountPath': '/etc/kubernetes/admin.conf',
                         'name': 'kubeconfig'}
                    ]
                }
            ],
            'hostNetwork': True,
            'hostAliases': [
                {'hostnames': ['kubernetes'],
                 'ip': '127.0.0.1'}
            ],
            'volumes': [
                {
                    'hostPath': {
                        'path': '/etc/kubernetes/admin.conf',
                        'type': 'FileOrCreate'
                    },
                    'name': 'kubeconfig'
                }
            ]
        },
        'status': {}
    }

    files: List[config_util.YamlObj] = [
        {
            'path': '/etc/kubernetes/manifests/kube-vip.yaml',
            'owner': 'root:root',
            'content': config_util.literal(config_util.dump(content))
        }
    ]
    spec: config_util.YamlDict = {
        'clusterConfiguration': {
            'apiServer': {
                'certSANs': [
                    'localhost',
                    '127.0.0.1',
                    '0.0.0.0',
                    'host.docker.internal'
                ]
            },
            'controllerManager': {
                'extraArgs': {
                    'enable-hostpath-provisioner': "true"
                }
            }
        },
        'initConfiguration': {
            'nodeRegistration': {
                'criSocket': '/var/run/containerd/containerd.sock',
                'ignorePreflightErrors': [
                    'Swap',
                    'DirAvailable--etc-kubernetes-manifests',
                    'FileAvailable--etc-kubernetes-kubelet.conf'
                ]
            }
        },
        'joinConfiguration': {
            'nodeRegistration': {
                'criSocket': '/var/run/containerd/containerd.sock',
                'ignorePreflightErrors': [
                    'Swap',
                    'DirAvailable--etc-kubernetes-manifests',
                    'FileAvailable--etc-kubernetes-kubelet.conf'
                ]
            }
        },
        'files': files
    }
    return spec


def kubeadmconfig_manifest(
    cluster: updater.Cluster, name: str
) -> config_util.YamlDict:
    manifest: config_util.YamlDict = {
        'kind': 'KubeadmConfigTemplate',
        'apiVersion': updater.BOOTSTRAP_GROUP,
        'metadata': cluster.metadata(f'md-{cluster.cluster_name}-{name}'),
        'spec': {
            'template': {
                'spec': {}
            }
        }
    }
    return manifest
