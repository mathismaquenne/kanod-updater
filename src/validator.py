#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import jsonschema   # type: ignore
import logging
import requests
import yaml

import config_util

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


COMMON_SCHEMA = {
    'type': 'object',
    'properties': {
        'os': {
            'type': 'string',
            'description': (
                'Operating system name (bionic/centos-8) of a K8s node')
        },
        'version': {
            'type': 'string',
            'description': 'kubernetes version'
        },
        'os_version': {
            'type': 'string',
            'description': (
                'explicit version of the OS image stored on the Nexus.'
                'assume latest if not specified')
        },
        'imperative': {
            'type': 'object',
            'description': 'Imperative way of configuring machines',
            'properties': {
                'run_cmds': {
                    'type': 'array',
                    'description': 'cloud-init standard runcmd commands',
                    'items': {
                        'type': 'string'
                    },
                },
                'post_cmds': {
                    'type': 'array',
                    'description': (
                        'cloud-init standard runcmd commands '
                        'launched after kubeadm'),
                },
                'kanod_configure': {
                    'type': 'boolean',
                    'description': 'optional switch to disable kanod_configure'
                }
            },
        },
        'vault': {
            'type': 'object',
            'properties': {
                'url': {
                    'type': 'string',
                    'description': 'vault endpoint'
                },
                'role': {
                    'type': 'string',
                    'description': 'role of the vault token to get'
                },
                'role_id': {
                    'type': 'string',
                    'description': (
                        'The role-id of the role must match what is set '
                        'in vault.')
                },
                'secret_id': {
                    'type': 'string',
                    'description': (
                        'For approle. Usually supplied by boot utility')
                },
                'ca': {
                    'type': 'string',
                    'description': (
                        'Certificate used to connect to vault.'
                        'It will be considered as trusted later on.'
                    )
                },
                'certificates': {
                    'type': 'array',
                    'description': 'Array of certificates to create',
                    'items': {
                        'type': 'object',
                        'properties': {
                            'name': {
                                'type': 'string',
                                'description': 'name of the certificate',
                            },
                            'ip': {
                                'type': 'array',
                                'items': {
                                    'type': 'string'
                                },
                                'description': 'Array of SAN IPs'
                            },
                            'alt_names': {
                                'type': 'array',
                                'items': {
                                    'type': 'string'
                                },
                                'description': 'Array of SAN IPs'
                            }

                        }
                    },
                }
            }
        },
    }
}


def is_object(schema):
    return schema.get('type', None) == 'object'


def augment_schema(schema, aux):
    if (not is_object(schema) or not is_object(aux)):
        raise Exception('Non mergeable schemas')
    schema.setdefault('properties', {}).update(aux.get('properties', {}))
    schema.setdefault('required', []).extend(aux.get('required', []))


def find_schema(image, cluster, verify):
    downloadurl = f'{image}-schema.yaml'
    resp = requests.get(downloadurl, verify=verify)
    if resp.status_code < 299:
        schema_text = resp.content.decode('utf-8')
        try:
            schema = yaml.safe_load(schema_text)
        except Exception as e:
            raise Exception(f'Cannot parse schema at {downloadurl}: {e}')
    else:
        log.info('Did not find a correct schema at %s', downloadurl)
        schema = None
    return schema


class ExtendedFormatChecker(jsonschema.FormatChecker):
    '''Permissive checker for manifest customization

    This format checker handles the cases where the value comes from Vault or
    the value is defined as an instance metadata
    '''
    def __init__(self):
        super().__init__({})

    def check(self, instance: object, format: str) -> None:
        '''Check an instance follows the format.

        Defer to jsonschema7 format checker unless it is a Vault reference
        or a meta data reference
        '''
        if isinstance(instance, str):
            if (instance.startswith('@vault:') or
                    (instance.startswith('{{') and instance.endswith('}}'))):
                return
        jsonschema.draft7_format_checker.check(instance, format)

    def conforms(self, instance: object, format: str) -> bool:
        try:
            self.check(instance, format)
        except jsonschema.FormatError:
            return False
        else:
            return True


def validate(
    error_handler: config_util.ErrorHandler,
    kind: str,
    validator: jsonschema.Draft7Validator,
    yaml_struct
):
    log.info('%s - starting validation', kind)
    for error in validator.iter_errors(yaml_struct):
        msg = error.message.replace('\n', '\n  ')
        path = '.'.join([str(e) for e in error.absolute_path])
        msg = f'* {msg}  at {"." if path == "" else path}'
        error_handler.error(f'Validation {kind}', msg)
    else:
        if len(error_handler.errors) == 0:
            log.info('%s: validation succeeded', kind)


def validate_node(
    error_handler: config_util.ErrorHandler, image, config, verify
):
    schema = find_schema(image, config, verify)
    if schema is None:
        error_handler.warning(
            'validation', 'Cannot find the validation json schema')
        return
    augment_schema(schema, COMMON_SCHEMA)
    validator = jsonschema.Draft7Validator(
        schema,
        format_checker=ExtendedFormatChecker())
    validate(error_handler, 'node', validator, config)


def get_schema():
    with open('infra_schema.yaml', 'r') as file:
        return yaml.safe_load(file)


def validate_infra(error_handler: config_util.ErrorHandler, infra):
    schema = get_schema()
    schema['$ref'] = '#/definitions/infra_definition'
    validator = jsonschema.Draft7Validator(
        schema,
        format_checker=ExtendedFormatChecker())
    validate(error_handler, 'infra', validator, infra)


def validate_cluster(error_handler, spec):
    schema = get_schema()
    schema['$ref'] = '#/definitions/cluster_definition'
    validator = jsonschema.Draft7Validator(
        schema,
        format_checker=ExtendedFormatChecker())
    validate(error_handler, 'cluster', validator, spec)
