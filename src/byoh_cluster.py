#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import Dict  # noqa: H301

import config_util
import updater


def byoh_cluster_manifest(cluster: updater.Cluster) -> config_util.YamlDict:
    spec: Dict[str, config_util.YamlObj] = {
        'bundleLookupBaseRegistry': '',
        'controlPlaneEndpoint': {
            'host': cluster.endpoint_host,
            'port': cluster.endpoint_port
        },
    }
    manifest: config_util.YamlDict = {
        'apiVersion': updater.BYOH_GROUP,
        'kind': 'ByoCluster',
        'metadata': cluster.metadata(
            cluster.cluster_name),
        'spec': spec
    }
    return manifest
