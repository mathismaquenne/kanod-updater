#  Copyright (C) 2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Optional  # noqa: F401,H301

import logging
import yaml

import config_util
import kubeadm
import machines
import updater
import validator

log = logging.getLogger(__name__)


def rke2_config_spec(
    config: config_util.YamlDict,
    cluster: Optional[updater.Cluster]
) -> config_util.YamlDict:

    '''Generates the spec part of RKE2ConfigTemplate

    It is also used by the rke2 controlplane.

    :param content: inlined instance of the yaml file describing the
       configuration of node as a unix machine
    '''

    imperative = cast(config_util.YamlDict, config.get('imperative', {}))
    pre_kube = cast(List[str], imperative.get('run_cmds', []))
    kanod_configure = imperative.get('kanod_configure', True)
    content = (config_util.dump(config) if kanod_configure else '')
    files: List[config_util.YamlObj] = [
        {'path': '/etc/kanod-configure/configuration.yaml',
         'content': content}]
    spec: config_util.YamlDict = {
        'agentConfig': {
            'nodeName': '{{ ds.meta_data.local_hostname }}',
            'kubeletArgs': ['provider-id=metal3://{{ ds.meta_data.uuid }}']
        },
        'deployRKE2Commands': cast(
            config_util.YamlObj,
            ['HOME=/root', 'kanod-runcmd'] + pre_kube if kanod_configure
            else pre_kube),
        'files': files
    }
    if cluster is not None:
        controlplane = cluster.k8sconfig
        server_config = {
            'advertiseAddress': cluster.endpoint_host,
        }
        cert_sans = cast(
            Optional[List[str]],
            cast(config_util.YamlDict,
                 controlplane.get('apiServer', {})).get('certSANs', None))
        if cert_sans is not None:
            server_config['tlsSan'] = cert_sans
        for (componentName, key, prefix) in [
            ('apiServer', 'kubeAPIServer', 'apis'),
            ('scheduler', 'kubeScheduler', 'sched'),
            ('controllerManager', 'kubeControllerManager', 'cm')
        ]:
            component = cast(
                config_util.YamlDict, controlplane.get(componentName, None))
            if component is None:
                continue
            params = {}
            args = component.get('extraArgs', None)
            if args is not None:
                params['args'] = args
            volumes = component.get('volumes', None)
            if volumes is not None:
                vols_spec: List[config_util.YamlObj] = []
                params['extraVolumes'] = vols_spec
                i = 0
                for volume in cast(List[config_util.YamlDict], volumes):
                    mount_path = volume.get('mountPath', None)
                    file_content = volume.get('content', None)
                    readOnly = volume.get('readonly', False)
                    ro = ':ro' if readOnly else ''
                    if not isinstance(mount_path, str):
                        continue
                    vol_name = f'{prefix}-{i}'
                    host_path = f'/etc/kanod-configure/k8s-{vol_name}'
                    vols_spec.append(
                        cast(config_util.YamlObj,
                             f'{host_path}:{mount_path}{ro}'))
                file_spec: config_util.YamlDict = {
                    'path': host_path,
                }
                if file_content is not None:
                    file_spec['content'] = file_content
                files.append(file_spec)
                i += 1

            server_config[key] = params
        localEndpoint = controlplane.get('localEndPoint', None)
        if isinstance(localEndpoint, dict):
            host = localEndpoint.get('host', None)
            port = localEndpoint.get('port', 6443)
            if isinstance(host, str) and isinstance(port, int):
                server_config['bindAddress'] = host
                server_config['httpsListenPort'] = port
        spec['serverConfig'] = server_config
    return spec


def rke2_controlplane_manifest(
    cluster: updater.Cluster, version: str
) -> config_util.YamlDict:
    cp_spec = cluster.controlplane
    # inherited during merge from customer spec
    replicas = cast(int, cp_spec.get('replicas', 1))
    server_config = cast(config_util.YamlDict, cp_spec.get('serverConfig', {}))
    config = kubeadm.enhance_config(cluster, server_config)
    cluster.vault.inject('cp', config)
    image = machines.image_path(cluster, config, version)
    validator.validate_node(
        cluster.error_handler, image, config, cluster.verify)
    baremetal = cast(
        config_util.YamlDict, cp_spec.get('hostSelector', {}))
    uid = updater.build_uid(f'{image}{yaml.dump(baremetal, sort_keys=True)}')
    spec: config_util.YamlDict = {
        'replicas': replicas,
        'version': f'{version}+rke2r1',
        'infrastructureTemplate': {
            'kind': 'Metal3MachineTemplate',
            'apiVersion': updater.CAPM3_GROUP,
            'name': f'controlplane-{cluster.cluster_name}-{uid}'
        },
        'rke2ConfigSpec': rke2_config_spec(config, cluster)
    }
    manifest: config_util.YamlDict = {
        'kind': 'RKE2ControlPlane',
        'apiVersion': updater.RKE2_CP_GROUP,
        'metadata': cluster.metadata(f'controlplane-{cluster.cluster_name}'),
        'spec': spec
    }
    return manifest


def rke2config_manifest(
    cluster: updater.Cluster, md_spec: config_util.YamlDict,
    version: str
) -> config_util.YamlDict:
    name = cast(str, md_spec.get('name', ''))
    config = cast(config_util.YamlDict, md_spec.get('serverConfig', {}))
    config = kubeadm.enhance_config(cluster, config)
    cluster.vault.inject(f'md-{name}', config)
    image = machines.image_path(cluster, config, version)
    validator.validate_node(
        cluster.error_handler, image, config, cluster.verify)
    # extra_args = cast(Dict[str, str], md_spec.get('kubeletExtraArgs', {}))
    specTemplate = rke2_config_spec(config, None)
    spec: config_util.YamlDict = {'template': {'spec': specTemplate}}
    manifest: config_util.YamlDict = {
        'kind': 'RKE2ConfigTemplate',
        'apiVersion': updater.RKE2_BOOTSTRAP_GROUP,
        'metadata': cluster.metadata(f'md-{cluster.cluster_name}-{name}'),
        'spec': spec
    }
    return manifest
