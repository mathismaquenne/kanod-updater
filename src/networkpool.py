#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from hashlib import sha256
from typing import cast, Dict, List  # noqa: F401,H301

import logging

import config_util
import updater

log = logging.getLogger(__name__)


def networkpool_manifest(
    cluster: updater.Cluster,
    netpool: config_util.YamlDict,
    map_network: Dict[str, Dict],
    ipam_networks: Dict[str, str]
    ) -> List[config_util.YamlDict]:

    networkid = cast(str, netpool.get('networkId', ""))
    secretNamePrefix = f'{cluster.cluster_name}-{networkid}'
    secretNamePrefixsha256 = sha256(secretNamePrefix.encode('ascii'))
    secretNamePrefixsha256Hex = secretNamePrefixsha256.hexdigest()
    secretName = f'{secretNamePrefixsha256Hex[:10]}-broker-secret'

    brokerAddress = ""
    if networkid in map_network:
        brokerAddress = map_network[networkid]["address"]
        networkdefName = map_network[networkid]["networkdefName"]
        brokerConfig = map_network[networkid]["brokerConfig"]
    else:
        msg = f'missing key {networkid} in cluster-def spec.networkList'
        raise Exception(msg)

    dhcpMode = cast(str, netpool.get('dhcpMode', 'none'))

    spec = {
        'networkDefinitionName': networkdefName,
        'networkDefinitionCredentialName': secretName,
        'dhcpMode': dhcpMode,
        'brokernetAddress': brokerAddress,
        'bareMetalPoolList': netpool.get('bareMetalPoolList', []),
        'brokerConfig': brokerConfig,
    }

    net_name = cast(str, netpool.get('name', ""))
    ippool = ipam_networks.get(net_name, None)
    if ippool is not None:
        spec['ipPool'] = ippool
        spec['cluster'] = cluster.cluster_name

    manifest: config_util.YamlDict = {
        'kind': 'Network',
        'apiVersion': updater.NETPOOL_GROUP,
        'metadata':
            cluster.metadata(f'{net_name}'),
        'spec': spec
    }
    return [manifest]
