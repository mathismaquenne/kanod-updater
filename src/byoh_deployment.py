#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, List, Optional  # noqa: F401,H301

import logging
import yaml

import byoh_kubeadm
import byoh_machines
import config_util
import updater
import versions

log = logging.getLogger(__name__)


def machinedeployment_manifest(
    cluster: updater.Cluster, name: str, md_spec: config_util.YamlDict,
    version: str
) -> config_util.YamlDict:
    replicas = md_spec.get('replicas', 1)
    replicas_bounds = cast(
        Optional[config_util.YamlDict], md_spec.get('replicasBetween', None))
    config = cast(config_util.YamlDict, md_spec.get('serverConfig', {}))
    image = byoh_machines.image_path(cluster, config, version)
    baremetal = cast(
        config_util.YamlDict, md_spec.get('hostSelector', {}))
    uid = updater.build_uid(f'{image}{yaml.dump(baremetal, sort_keys=True)}')
    base_labels = {
        'cluster.x-k8s.io/cluster-name': cluster.cluster_name,
        'nodepool': 'standard-nodepool'
    }
    filter = {**base_labels, 'kanod.io/md-name': name}
    pool = cast(Optional[str], md_spec.get('pool', None))
    labels = cast(Dict[str, str], cast(config_util.YamlDict, base_labels))
    if pool is not None:
        labels = {**labels, 'kanod.io/baremetalpool': pool}

    template: config_util.YamlDict = {
        'metadata': {
            'labels': cast(config_util.YamlDict, filter.copy())
        },
        'spec': {
            'clusterName': cluster.cluster_name,
            'version': version,
            'bootstrap': {
                'configRef': {
                    'name': f'md-{cluster.cluster_name}-{name}',
                    'apiVersion': updater.BOOTSTRAP_GROUP,
                    'kind': 'KubeadmConfigTemplate'
                }
            },
            'infrastructureRef': {
                'name': f'md-{cluster.cluster_name}-{name}-{uid}',
                'apiVersion': updater.CAPM3_GROUP,
                'kind': 'ByoMachineTemplate'
            }
        }
    }
    spec: config_util.YamlDict = {
        'clusterName': cluster.cluster_name,
        'replicas': replicas,
        'selector': {
            'matchLabels': cast(config_util.YamlDict, filter.copy())
        },
        'template': template
    }
    if replicas is not None:
        spec['replicas'] = replicas
    if replicas_bounds is not None:
        min = str(replicas_bounds.get('min', 1))
        max = str(replicas_bounds.get('max', 1))
        annotations = {
            'cluster.x-k8s.io/cluster-api-autoscaler-node-group-min-size': min,
            'cluster.x-k8s.io/cluster-api-autoscaler-node-group-max-size': max
        }
        cluster.need_autoscaler = True
    else:
        annotations = None

    manifest: config_util.YamlDict = {
        'kind': 'MachineDeployment',
        'apiVersion': updater.CAPI_GROUP,
        'metadata':
            cluster.metadata(f'md-{cluster.cluster_name}-{name}',
                             labels=labels, annotations=annotations),
        'spec': spec
    }

    return manifest



def full_deployment(
    cluster: updater.Cluster, md_spec: config_util.YamlDict, metadata_name: str
) -> List[config_util.YamlDict]:
    name: Optional[str] = cast(Optional[str], md_spec.get('name', None))
    if name is None:
        raise Exception('Machine deployments must have a name')
    version = versions.deployment_version(cluster, name, md_spec)
    if version is None:
        message = ('Cannot proceed without Kubernetes version for'
                   f'deployment {name} of {cluster.cluster_name}')
        log.error(message)
        cluster.error_handler.error('validation', message)
        return []
    md_manifest = machinedeployment_manifest(cluster, name, md_spec, version)
    kac_manifest = byoh_kubeadm.kubeadmconfig_manifest(cluster, name)
    byoh_machine_manifest = byoh_machines.machine_template_manifest(
        cluster, name, md_spec, metadata_name, version)
    return [md_manifest, byoh_machine_manifest, kac_manifest]

