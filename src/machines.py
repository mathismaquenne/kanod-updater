#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Optional  # noqa: H301

import yaml

import config_util
import updater


def image_path(
    cluster: updater.Cluster, spec: config_util.YamlDict,
    version: Optional[str]
) -> str:
    os = 'rke2' if cluster.rke2 else cast(str, spec.get('os', 'bionic'))
    img_version = cast(str, spec.get('os_version', '1.0.0'))
    if version is None:
        cluster.error_handler.error(
            'configuration',
            'Cannot proceed without a valid kubernetes version for all nodes.')
        version = ''
    # TODO(pc): we would be more maven independent if repository/kanod/kanod
    # was in the spec.
    image = (
        f'{cluster.maven}/repository/kanod/kanod/'
        f'{os}-{version}/{img_version}/{os}-{version}-{img_version}')
    return image


def machine_template_manifest(
    cluster: updater.Cluster, name: Optional[str],
    cp_spec: config_util.YamlDict,
    metadata_name: str, version: str
) -> config_util.YamlDict:
    image_format = cast(str, cp_spec.get('imageFormat', 'qcow2'))
    selector = cast(
        Optional[config_util.YamlDict], cp_spec.get('hostSelector', {}))
    image = image_path(
        cluster,
        cast(config_util.YamlDict, cp_spec.get('serverConfig', {})),
        version)
    uid = updater.build_uid(f'{image}{yaml.dump(selector, sort_keys=True)}')
    machine_name = (
        f'controlplane-{cluster.cluster_name}-{uid}' if name is None
        else f'md-{cluster.cluster_name}-{name}-{uid}'
    )
    specTemplate: config_util.YamlDict = {
        'image': {
            'format': image_format,
            'url': f'{image}.{image_format}',
            'checksum': f'{image}.{image_format}.md5'
        },
        'dataTemplate': {'name': metadata_name}
    }
    if cluster.vault.use_tpm():
        if selector is None:
            selector = {}
        mlbls = cast(
            config_util.YamlDict, selector.setdefault('matchLabels', {}))
        mlbls['kanod.io/tpm'] = 'registered'
    pool = cast(Optional[str], cp_spec.get('pool', None))
    if pool is not None:
        if selector is None:
            selector = {}
        mlbls = cast(
            config_util.YamlDict, selector.setdefault('matchLabels', {}))
        mlbls['kanod.io/baremetalpool'] = pool

    if selector is not None:
        specTemplate['hostSelector'] = selector
    spec: config_util.YamlDict = {
        'template': {'spec': specTemplate}
    }
    manifest: config_util.YamlDict = {
        'apiVersion': updater.CAPM3_GROUP,
        'kind': 'Metal3MachineTemplate',
        'metadata': cluster.metadata(machine_name),
        'spec': spec
    }
    return manifest
