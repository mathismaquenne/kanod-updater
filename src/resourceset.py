#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Optional  # noqa: H301

import requests

import config_util
import dictsubst
import updater


def extract_templates(
    config: config_util.YamlDict, entry: str
) -> Dict[str, config_util.YamlDict]:
    pre_templates = cast(List[config_util.YamlDict], config.get(entry, []))
    result = {
        (cast(str, template['name'])): template
        for template in pre_templates
        if 'name' in template
    }
    for template in pre_templates:
        template.pop('name', None)
    return result


def clusterresourceset_manifest(
    cluster: updater.Cluster, resources: List[str]
) -> List[config_util.YamlDict]:

    '''Builds a resourceset and the configmap containing the manifests'''
    rs_spec: config_util.YamlDict = {
        'clusterSelector': {
            'matchLabels': {
                'kanod.io/cluster_name': cluster.cluster_name
            }
        },
        'resources': [
            {'name': f'rs-{cluster.cluster_name}-{i}', 'kind': 'ConfigMap'}
            for i in range(len(resources))
        ]
    }
    rs_manifest: config_util.YamlDict = {
        'apiVersion': f'addons.{updater.CAPI_GROUP}',
        'kind': 'ClusterResourceSet',
        'metadata': cluster.metadata(f'rs-{cluster.cluster_name}'),
        'spec': rs_spec
    }

    cm_manifests: List[config_util.YamlDict] = [
        {
            'apiVersion': 'v1',
            'kind': 'ConfigMap',
            'metadata': cluster.metadata(
                f'rs-{cluster.cluster_name}-{i}',
                annotations={
                    'argocd.argoproj.io/sync-options': 'Replace=true'}),
            'data': {"res": resource}
        }
        for i, resource in enumerate(resources)]
    return [rs_manifest] + cm_manifests


def variables_enrich(
    cluster: updater.Cluster, variables: Dict[str, str], version: str
):
    '''Builds a set of shell variables for customization of resources'''
    variables['ENDPOINT_PORT'] = str(cluster.endpoint_port)
    variables['VERSION'] = version
    variables['ENDPOINT_HOST'] = cluster.endpoint_host
    variables['CLUSTER_NAME'] = cluster.cluster_name
    if cluster.maven is not None:
        variables['NEXUS'] = cluster.maven
    if cluster.docker is not None:
        variables['NEXUS_REGISTRY'] = cluster.docker


def generate_inlined_addons(
    cluster: updater.Cluster, cp_version: str
) -> List[config_util.YamlDict]:
    '''Generates the cluster addons.

    This version leverages cluster-api optional ClusterResourceSet CRD.
    The resources must be available on the web and are env-substituted
    with a set of fixed variables.
    '''
    updater.log.info('addons generation %s', cluster.cluster_name)
    flavour_names = cast(
        List[str], cluster.cluster_config.get('addons', []))
    addons_templates = extract_templates(
        cluster.infra_config, 'addonFlavours')
    resource_list = []
    for flavour_name in flavour_names:
        flavour = addons_templates.get(flavour_name, None)
        if flavour is None:
            cluster.error_handler.warning(
                'addons', f'Could not find addon flavour {flavour_name}')
            continue
        variables = cast(Dict[str, str], flavour.get('variables', {}))
        variables_enrich(cluster, variables, cp_version)
        envsubst = dictsubst.dictsubst(variables)
        resources = cast(Optional[List[config_util.YamlDict]],
                         flavour.get('resources', {}))

        if resources is not None:
            for resource in resources:
                url = cast(Optional[str], resource.get('url', None))
                if url is None:
                    name = cast(Optional[str], resource.get('name', None))
                    version = cast(str, resource.get('version', '1.0.0'))
                    if name is not None:
                        url = (
                            f'${{NEXUS}}/repository/kanod/kanod'
                            f'/{name}/{version}/{name}-{version}.yaml')
                    else:
                        continue
                url = cast(str, envsubst.substitute(url))
                resp = requests.get(url, verify=cluster.verify)
                if resp.status_code > 299:
                    cluster.error_handler.warning(
                        'addons',
                        f'addon {flavour_name}: problem to load resource'
                        f' at {url}'
                    )
                    updater.log.error("cannot load addon resource at f{url}")
                    continue
                content = resp.content.decode('utf-8')
                resource_list.append(envsubst.substitute(content))
    if resource_list == []:
        return []
    rs = clusterresourceset_manifest(cluster, resource_list)
    return rs
