#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from hashlib import sha256
from typing import cast, Dict, List, NamedTuple, Optional  # noqa: F401,H301

import logging

import config_util
import updater

log = logging.getLogger(__name__)


BmpBind = NamedTuple(
    'BmpBind',
    [('networks', List[str]), ('bindings', List[str]),
     ('ippools', Dict[str, str]), ('pxe', List[str])])


def get_pool_network_map(
    cluster: updater.Cluster
) -> Dict[str, BmpBind]:
    network_ippool_map: Dict[str, str] = {}
    for ippool in cast(List[config_util.YamlDict], cluster.ippools):
        ippool_name = cast(Optional[str], ippool.get('name', None))
        net_name = cast(Optional[str], ippool.get('network', None))
        if ippool_name is None or net_name is None:
            continue
        network_ippool_map[net_name] = ippool_name
    pool_network_map: Dict[str, BmpBind] = {}
    for netpool in cast(List[config_util.YamlDict], cluster.networkpools):
        bmps = cast(
            List[config_util.YamlDict],
            netpool.get("bareMetalPoolList", []))
        net_name = cast(Optional[str], netpool.get('name', None))
        if net_name is None:
            continue
        ippool_name = network_ippool_map.get(net_name, None)
        pxe = (cast(str, netpool.get('dhcpMode', 'none')) == 'pxe')
        for elt in bmps:
            name = cast(Optional[str], elt.get('name', None))
            binding_name = cast(Optional[str], elt.get('bindingName', None))
            if name is None or binding_name is None:
                continue
            bmp_bind = pool_network_map.setdefault(
                name,
                BmpBind(networks=[], bindings=[], ippools={}, pxe=[]))
            bmp_bind.networks.append(net_name)
            bmp_bind.bindings.append(binding_name)
            if ippool_name is not None:
                bmp_bind.ippools[binding_name] = ippool_name
            if pxe:
                bmp_bind.pxe.append(binding_name)
    return pool_network_map


def bmh_data_manifest(
    cluster: updater.Cluster,
    name: str,
    bmhData: config_util.YamlDict,
    byohInPool: bool,
) -> List[config_util.YamlDict]:

    spec: config_util.YamlDict = {
    }

    if byohInPool:
        spec['generateMetadata'] = True

    poollist = cast(Dict[str, str], bmhData.get('ipPools', []))
    if (len(poollist)) > 0:
        for (category, pre) in [
            ('ipAddressesFromIPPool', 'ip'),
            ('prefixesFromIPPool', 'prefix'),
            ('gatewaysFromIPPool', 'gateway'),
            ('dnsServersFromIPPool', 'dns')
        ]:
            spec[category] = [
                {'key': f'{pre}_{key}', 'name': ippool_name,
                 'apiGroup': 'ipam.metal3.io', 'kind': 'IPPool'}
                for key, ippool_name in poollist.items()
            ]

    for (category, entryKey) in [
            ('ipAddressesFromIPPool', 'ipAddresses'),
            ('prefixesFromIPPool', 'prefixes'),
            ('gatewaysFromIPPool', 'gateways'),
            ('dnsServersFromIPPool', 'dnsServers')
    ]:
        entries = cast(Dict[str, str], bmhData.get(entryKey, {}))
        items: List[config_util.YamlObj] = [
            {'key': key, 'name': ippool_name,
             'apiGroup': 'ipam.metal3.io', 'kind': 'IPPool'}
            for key, ippool_name in entries.items()
        ]
        spec[category] = (
            cast(List[config_util.YamlObj], spec.get(category, [])) + items)

    for (category, entryKey, item) in [
        ('fromLabels', 'labels', 'label'),
        ('fromAnnotations', 'annotations', 'annotation')
    ]:
        entries = cast(Dict[str, str], bmhData.get(entryKey, {}))
        if len(entries) > 0:
            cat_items: List[config_util.YamlObj] = [
                {'key': key, item: val}
                for key, val in entries.items()
            ]
            spec[category] = cat_items

    manifest: config_util.YamlDict = {
        'kind': 'BmhData',
        'apiVersion': 'network.kanod.io/v1beta1',
        'metadata': cluster.metadata(f'bmh-data-{name}'),
        'spec': spec
    }
    result = [manifest]
    bootNetworkTemplate = cast(
        Optional[str], bmhData.get('bootNetwork', None))
    if bootNetworkTemplate is not None:
        spec['preprovisioningNetwork'] = f'boot-network-{name}'
        secret: config_util.YamlDict = {
            'kind': 'Secret',
            'apiVersion': 'v1',
            'metadata': cluster.metadata(f'boot-network-{name}'),
            'stringData': {
                'template': bootNetworkTemplate
            }
        }
        result.append(secret)
    networkDataTemplate = cast(
        Optional[str], bmhData.get('networkData', None))
    if networkDataTemplate is not None:
        spec['networkData'] = f'network-data-{name}'
        net_secret: config_util.YamlDict = {
            'kind': 'Secret',
            'apiVersion': 'v1',
            'metadata': cluster.metadata(f'network-data-{name}'),
            'stringData': {
                'template': networkDataTemplate
            }
        }
        result.append(net_secret)
    return result



def baremetalpool_manifest(
    cluster: updater.Cluster,
    bmpool: config_util.YamlDict,
    map_pool_user: Dict[str, Dict],
    map_pool_networks: Dict[str, BmpBind],
    version: str
) -> List[config_util.YamlDict]:
    bm_name = cast(str, bmpool.get('name', ''))
    brokerid = cast(str, bmpool.get('brokerId', ''))
    bmhlabels = cast(Dict[str, str], bmpool.get('labels', {}))
    bmhannotations = cast(Dict[str, str], bmpool.get('annotations', {}))
    blockingAnnotations = cast(
        List[str], bmpool.get('blockingAnnotations', []))
    onlineMode = cast(bool, bmpool.get('online', False))

    secretNamePrefix = f'{cluster.cluster_name}-{brokerid}'
    secretNamePrefixsha256 = sha256(secretNamePrefix.encode('ascii'))
    secretNamePrefixsha256Hex = secretNamePrefixsha256.hexdigest()
    secretName = f'{secretNamePrefixsha256Hex[:10]}-broker-secret'

    poolName = cast(str, bmpool.get('poolName', ''))
    brokerAddress = ''
    bmp_bind = map_pool_networks.get(bm_name, None)
    annotations = None

    byohInPool = False
    byohConfig = cast(
        config_util.YamlDict,
        bmpool.get('byoh', None))
    if byohConfig is not None:
        byohInPool = cast(bool, byohConfig.get('inPool', False))

    if byohInPool:
        cluster.need_byohostpoolscaler = True

    ip_addresses = cast(Dict[str, str], bmpool.get('ipAddresses', {}))
    if bmp_bind is not None:
        blockingAnnotations.extend(
            f'kanod.io/net-{b}-interface' for b in bmp_bind.bindings)
        annotations = {
            'baremetalpool.kanod.io/disabled': 'kanod.io/networks',
            'baremetalpool.kanod.io/mandatory-networks':
                ','.join(bmp_bind.networks)
        }
        ip_addresses = {**bmp_bind.ippools, **ip_addresses}
        if len(bmp_bind.pxe) == 1:
            bmhlabels['kanod.io/dhcp'] = bmp_bind.pxe[0]
            blockingAnnotations.append('dhcp.kanod.io/configured')
    if len(ip_addresses) > 0:
        ipam_annotations = (
            ', '.join(f'{k}: {v}' for (k, v) in ip_addresses.items()))
        bmhannotations['kanod.io/ipam'] = (
            ipam_annotations if 'kanod.io/ipam' not in bmhannotations
            else ipam_annotations + ', ' + bmhannotations['kanod.io/ipam'])
    bmhData = cast(
        Optional[config_util.YamlDict], bmpool.get('bmhData', None))
    if bmhData is not None:
        bmh_data = bmh_data_manifest(cluster, poolName, bmhData,
                                     byohInPool)
    else:
        bmh_data = []

    if brokerid in map_pool_user:
        brokerAddress = map_pool_user[brokerid]['address']
        brokerConfig = map_pool_user[brokerid]['brokerConfig']
    else:
        msg = f'missing key {brokerid} in cluster-def spec.poolUserList'
        raise Exception(msg)

    spec = {
        'poolName': poolName,
        'credentialName': secretName,
        'address': brokerAddress,
        'redfishSchema': bmpool.get('redfishSchema', 'redfish'),
        'brokerConfig': brokerConfig,
        'online': onlineMode,
    }
    replicas = bmpool.get('replicas', None)
    if replicas is not None:
        spec['replicas'] = replicas
    for option in ['labelSelector', 'maxServers']:
        if option in bmpool:
            spec[option] = bmpool[option]
    if bmhData is not None:
        bmhlabels['network.kanod.io/bmh-data'] = f'bmh-data-{poolName}'
        blockingAnnotations.append('network.kanod.io/bmh-data-version')

    if byohInPool:
        userDataName = f'{bm_name}-userdata'
        userMetaData = cluster.metadata(userDataName)
        spec['userData'] = userMetaData

    if len(bmhlabels) > 0:
        spec['bmhLabels'] = bmhlabels

    if len(bmhannotations) > 0:
        spec['bmhAnnotations'] = bmhannotations

    if len(blockingAnnotations) > 0:
        spec['blockingAnnotations'] = blockingAnnotations

    manifest: config_util.YamlDict = {
        'kind': 'BareMetalPool',
        'apiVersion': updater.BAREMETALPOOL_GROUP,
        'metadata':
            cluster.metadata(bm_name, annotations=annotations),
        'spec': spec
    }

    return [manifest] + bmh_data



def byohostpool_scaler(
    cluster: updater.Cluster,
) -> List[config_util.YamlDict]:

    byohConfig = cast(Optional[config_util.YamlDict],
                      cluster.byohConfig)

    if byohConfig is None:
        raise Exception('byoh global configuration missing')

    byohOs = cast(str, byohConfig.get('os', 'bionic'))
    byohOsVersion = cast(str, byohConfig.get('osVersion', '1.0.0'))
    byohImageFormat = byohConfig.get('imageFormat', "qcow2")
    apiServer = byohConfig.get('apiServer', "https://192.168.133.10:6443")

    manifest: config_util.YamlDict = {
        'kind': 'ByoHostPoolScaler',
        'apiVersion': 'pools.kanod.io/v1alpha1',
        'metadata': cluster.metadata(f'{cluster.cluster_name}'),
        'spec': {
            'cluster': f'{cluster.cluster_name}',
            'byohOs': byohOs,
            'byohOsVersion': byohOsVersion,
            'byohImageFormat': byohImageFormat,
            'byohRepository': f'{cluster.maven}',
            'apiServer': apiServer,
            'insecureSkipTLSVerify': False,

        }
    }

    result = [manifest]

    return result
