#  Copyright (C) 202 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Optional, Union  # noqa: H301

import logging

import config_util

log = logging.getLogger(__name__)

strategy_type = Dict[  # type: ignore[misc]
    str, Union[str, bool, 'strategy_type']]  # type: ignore[misc]
strategy_map_type = Dict[str, Union[str, strategy_type]]  # type: ignore[misc]

'''The key that introduces strategy in json dictionaries. The strategy is
   a map from keys to strategy object. '''
STRATEGY_KEY = '$strategy'
'''item is forbidden (only makes sense in a strategy map to forbid elements)'''
FORBIDDEN_KEY = '$forbidden'
'''Template is just a default value and is overridden by specialization'''
DEFAULT_KEY = '$default'
'''Define the key on which dictionnary as array elements are merged.'''
MERGE_KEY = '$merge'
'''The template cannot be modified even if it is a dictionnary/array'''
FIXED_KEY = '$fixed'
'''No additional key can be added to the dictionnary'''
NO_ADDITIONAL_KEY = '$no_additional'
'''To specify the strategy of items during an array merge.'''
ITEM_KEY = '$item'


def merge(
    error_handler: config_util.ErrorHandler,
    strategy: strategy_type,
    template: Optional[config_util.YamlObj],
    spec: Optional[config_util.YamlObj]
) -> Optional[config_util.YamlObj]:
    '''Merge two yaml/json objects

    merge a template against a specialized version following a strategy.

    :param error_handler: an object providing error handling (errors and
        warnings)
    :param strategy: The strategy is itself a map of keys representing strategy
        option to their values. The value is usually a boolean except for the
        merge strategy on array.
    :param template: the base object that is completed
    :param spec: the object supplied by the client that specialize the template
    :returns: the result of specialization.
    '''
    def has_key(key: str):
        return strategy.get(key, False)

    if template is None:
        # we are in a case where we have at most the specialization. We keep it
        # unless it is forbidden to do so.
        return None if has_key(FORBIDDEN_KEY) else spec

    if spec is None or has_key(FIXED_KEY):
        # The only other case would be a strategy 'forbidden' with a template
        # This would be just meaningless.
        return template

    if type(spec) is not type(template):
        error_handler.error(
            'Structure',
            f'Trying to merge {type(template)} and {type(spec)}.')
        return None
    # mypy cannot use type(x) == type(y) as constraint.
    if isinstance(template, dict) and isinstance(spec, dict):
        return merge_dict(error_handler, strategy, template, spec)
    if isinstance(template, list) and isinstance(spec, list):
        if has_key(MERGE_KEY):
            return merge_array(
                error_handler, cast(str, strategy[MERGE_KEY]),
                strategy, template, spec)
        else:
            return template + cast(List[config_util.YamlObj], spec)
    # this is a constant. Give back the template unless it was explicitely a
    # default
    return spec if has_key(DEFAULT_KEY) else template


def merge_dict(
    error_handler: config_util.ErrorHandler,
    strategy,
    template: config_util.YamlDict,
    spec: config_util.YamlDict
) -> config_util.YamlDict:
    keys = {*template.keys(), *spec.keys()}
    result: config_util.YamlDict = {}
    strategy_map = cast(strategy_map_type, template.get(STRATEGY_KEY, {}))
    if not isinstance(strategy_map, dict):
        error_handler.warning(
            'merge', 'strategy should be a dictionary or a key')
        return template
    no_additional = strategy.get(NO_ADDITIONAL_KEY, False)
    for key in keys:
        if key == STRATEGY_KEY:
            continue
        item_strategy = strategy_map.get(key, {})
        if isinstance(item_strategy, str):
            item_strategy = {item_strategy: True}
        item_temp = template.get(key, None)
        if no_additional and item_temp is None:
            continue
        val = merge(
            error_handler, item_strategy,
            item_temp, spec.get(key, None))
        if val is not None:
            result[key] = val
    return result


def merge_array(
    error_handler: config_util.ErrorHandler, key: str,
    strategy,
    template: List[config_util.YamlObj],
    spec: List[config_util.YamlObj]
) -> List[config_util.YamlObj]:
    item_strategy = cast(Union[str, strategy_type], strategy.get(ITEM_KEY, {}))
    if isinstance(item_strategy, str):
        item_strategy = {item_strategy: True}
    map_spec = {
        elt[key]: elt
        for elt in spec
        if isinstance(elt, dict) and key in elt
    }
    upd_temp = [
        merge(error_handler, item_strategy, elt, map_spec[elt[key]])
        if isinstance(elt, dict) and elt.get(key, None) in map_spec
        else elt
        for elt in template
    ]
    filtered_temp = [
        e for e in upd_temp if e is not None
    ]
    keys_temp = {
        elt[key] for elt in template if isinstance(elt, dict) and key in elt
    }
    rem_spec = [
        cast(config_util.YamlObj, elt) for elt in spec
        if isinstance(elt, dict) and elt.get(key, None) not in keys_temp
    ]
    return filtered_temp + rem_spec


def simple_merge(
    parent: config_util.YamlDict,
    spec: config_util.YamlDict
) -> config_util.YamlDict:
    keys = {*parent.keys(), *spec.keys()}
    result: config_util.YamlDict = {}
    for key in keys:
        p_elt = parent.get(key, None)
        s_elt = spec.get(key, None)
        if p_elt is None:
            if s_elt is None:
                # should not occur, rather to silence mypy
                continue
            # not present in parent
            result[key] = s_elt
        elif s_elt is None:
            # not present in specialization
            result[key] = p_elt
        elif isinstance(p_elt, dict) and isinstance(s_elt, dict):
            # merge dictionary
            result[key] = simple_merge(p_elt, s_elt)
        else:
            # Priority goes to the specialization. Just override.
            # may make sense to raise an error.
            result[key] = s_elt
    return result
