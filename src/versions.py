#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

# Version handling for kubernetes clusters
#
# This files groups code that was previously scattered between the different
# components
#
# There are basically two important Kubernetes versions:
# * control-plane version
# * workers version
# The control-plane version should be the highest version deployed and never
# more than one unit higher than the lowest version deployed. Machine
# deployments may have different versions as long as they respect the previous
# rule.

from typing import cast, List, Optional  # noqa: H301

import logging
import re

from kubernetes import client  # type: ignore

import config_util
import updater

log = logging.getLogger(__name__)


def get_minor(s: str) -> int:
    '''From a semantic version written as v1.x.y returns x as an integer.'''
    m = re.match(r'v1\.([0-9]+)\.[0-9]+', s)
    if m is None:
        return 0
    return int(m.group(1))


def find_best_version(minor: int, versions: List[str]) -> Optional[str]:
    '''Gives back a version from versions matching minor

    It should be the highest version in the list.
    The result is None if no version is found.
    '''
    result = None
    for version in versions:
        if (get_minor(version) == minor and
                (result is None or version > result)):
            result = version
    return result


def get_current_k8s_version(
    conf_ns: str, cluster: updater.Cluster
):
    '''Extract from clusterdef status current kubelet versions

    We get the current control plane version and indirectly the machine
    deployment versions through all the kubelets of the different nodes.
    '''
    cluster_name = cluster.cluster_name
    customapi = client.CustomObjectsApi()
    clusterdef = customapi.get_namespaced_custom_object(
        'gitops.kanod.io', 'v1alpha1', conf_ns, 'clusterdefs', cluster_name)
    if clusterdef is None:
        log.warning("Clusterdef %s/%s not found", conf_ns, cluster_name)
        return
    status = clusterdef.get('status', {})
    cp_version = status.get('controlPlaneVersion', '')
    if cp_version != '':
        cluster.current_controlplane_version = cp_version
    machine_versions = status.get('machineVersions', [])
    cluster.machine_versions = machine_versions


def controlplane_version(cluster: updater.Cluster) -> Optional[str]:
    '''Kubernetes version for control plane

    Computes the highest kubernetes version of the control-plane compatible
    with cluster specification and already deployed kubelets.
    '''
    server = cast(
        config_util.YamlDict, cluster.controlplane.get('serverConfig', {}))
    version = cast(
        Optional[str],
        server.get(
            'version',
            cluster.cluster_config.get('version', None)))
    if version is None:
        return version
    current_versions = cluster.machine_versions
    if len(current_versions) > 0:
        # We assume (and clusterdef controller maintains it) that versions
        # are sorted in ascending order in current_versions.
        max_minor = get_minor(current_versions[0]) + 1
        if get_minor(version) > max_minor:
            cluster.limit_controlplane = max_minor
            potential_versions = cast(
                List[str],
                cluster.infra_config.get('supportedVersions', []))
            best_version = find_best_version(max_minor, potential_versions)
            log.warning(
                'Limiting version in control-plane %s from %s to %s',
                cluster.cluster_name, version, best_version)
            version = best_version
    return version


def deployment_version(
    cluster: updater.Cluster, name: str, spec: config_util.YamlDict
) -> Optional[str]:
    '''Kubernetes version for a machine deployment

    The version deployed in a machine deployment should not be greater than
    the version currently deployed in the control-plane
    '''
    # The code could be simplified if we removed the possibility to describe
    # kubernetes version in each machine deployment
    config = cast(config_util.YamlDict, spec.get('serverConfig', {}))
    version = cast(
        Optional[str],
        config.get('version', cluster.cluster_config.get('version', None)))
    # Cannot guess a version. Error recovery must be done.
    if version is None:
        return None
    if (cluster.current_controlplane_version is not None and
            cluster.current_controlplane_version < version):
        log.warning(
            'Limiting version in machine deployment %s - %s from %s to %s',
            cluster.cluster_name, name, version,
            cluster.current_controlplane_version)
        cluster.limit_workers = get_minor(cluster.current_controlplane_version)
        version = cluster.current_controlplane_version
    return version
