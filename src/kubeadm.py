#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from typing import cast, Dict, List, Optional  # noqa: F401,H301

import logging
import yaml

import config_util
import machines
import updater
import validator

log = logging.getLogger(__name__)


def enhance_config(
    cluster: updater.Cluster, config: config_util.YamlDict,
) -> config_util.YamlDict:
    '''Add a few fields to the config of servers'''
    return {
        **config,
        'nexus': cluster.nexus,
        'name': '{{ ds.meta_data.name }}'
    }


def enrich_kubelet_extra_args(
    cluster: updater.Cluster,
    extra_args: Dict[str, str], origin: str
):
    '''Enrich the kubelet extra arguments.

    Adds node labels and provider-id as computed for metal3.
    '''
    bm = cast(
        config_util.YamlDict,
        cluster.infra_config.get('baremetal', {}))
    exported = cast(List[str], bm.get('nodeLabels', []))
    node_labels = extra_args.get('node-labels', '').split(',')
    label_specs = (
        ['metal3.io/uuid={{ ds.meta_data.uuid }}',
            f'cluster.kanod.io/part-of={origin}'] +
        [
            f'baremetal.kanod.io/{name}={{{{ ds.meta_data.{name} }}}}'
            for name in exported
        ])
    return {
        'node-labels': ','.join(node_labels + label_specs),
        'provider-id': 'metal3://{{ ds.meta_data.providerid }}',
        **extra_args
    }


def kubeadm_config_spec(
    cluster: updater.Cluster,
    config: config_util.YamlDict,
    controlplane: Optional[config_util.YamlDict],
    key: str,
    kubelet_extra_args: Dict[str, str],
    taints: List[config_util.YamlObj]
) -> config_util.YamlDict:

    '''Generates the spec part of kubeadmconfig

    It is also used by the kubeadmcontrolplane. The case are distinguished by
    the presence of a controlplane argument and yields a clusterConfig and an
    initConfig.

    :param cluster: the cluster object containing the
    :param content: inlined instance of the yaml file describing the
       configuration of node as a unix machine
    :param controlplane: optional controlplane description.
    :param key: a prefix distinguisher
    :param kubelet_extra_args: optional arguments for the kubelet for both
       workers and control nodes
    '''

    imperative = cast(config_util.YamlDict, config.get('imperative', {}))
    pre_kube = cast(List[str], imperative.get('run_cmds', []))
    post_kube = cast(List[str], imperative.get('post_cmds', []))
    kanod_configure = imperative.get('kanod_configure', True)
    content = (config_util.dump(config) if kanod_configure else '')
    files: List[config_util.YamlObj] = [
        {'path': '/etc/kanod-configure/configuration.yaml',
         'content': content}]
    extra_args = enrich_kubelet_extra_args(cluster, kubelet_extra_args, key)
    nodeRegistration: config_util.YamlDict = {
        'name': '{{ ds.meta_data.name }}'
    }
    if len(extra_args) > 0:
        nodeRegistration['kubeletExtraArgs'] = extra_args.copy()
    if len(taints) > 0:
        nodeRegistration['taints'] = taints
    spec: config_util.YamlDict = {
        'joinConfiguration': {
            'nodeRegistration': nodeRegistration
        },
        'preKubeadmCommands': cast(
            config_util.YamlObj,
            ['HOME=/root', 'kanod-runcmd'] + pre_kube if kanod_configure
            else pre_kube),
        'postKubeadmCommands': cast(
            config_util.YamlObj,
            ['kanod-postcmd'] + post_kube if kanod_configure
            else post_kube),
        'files': files
    }
    if controlplane is not None:
        clusterConfiguration: config_util.YamlDict = {}
        if cluster.docker is not None:
            clusterConfiguration['imageRepository'] = cluster.docker
        initConfiguration: config_util.YamlDict = {
            'nodeRegistration': {
                'name': '{{ ds.meta_data.name }}',
                'kubeletExtraArgs': extra_args}}
        spec['initConfiguration'] = initConfiguration
        spec['clusterConfiguration'] = clusterConfiguration
        for (entry, prefix) in [('apiServer', 'apis'), ('scheduler', 'sched'),
                                ('controllerManager', 'cm')]:
            component = controlplane.get(entry, None)
            if component is None or not isinstance(component, dict):
                continue
            entry_content: config_util.YamlDict = {}
            clusterConfiguration[entry] = entry_content
            if entry in ['apiServer', 'apis']:
                certSANS = component.get('certSANs', None)
                if certSANS is not None:
                    entry_content['certSANs'] = certSANS
            extra_args = component.get('extraArgs', None)
            if extra_args is not None:
                entry_content['extraArgs'] = extra_args
            volumes = component.get('volumes', None)
            if volumes is None:
                continue
            vols_spec: List[config_util.YamlObj] = []
            entry_content['extraVolumes'] = vols_spec
            i = 0
            for volume in cast(List[config_util.YamlDict], volumes):
                mount_path = volume.get('mountPath', None)
                file_content = cast(str, volume.get('content', None))
                if not isinstance(mount_path, str):
                    continue
                vol_name = f'{prefix}-{i}'
                host_path = f'/etc/kanod-configure/k8s-{vol_name}'
                readOnly = volume.get('readonly', False)
                vols_spec.append({
                    'hostPath': host_path,
                    'mountPath': mount_path,
                    'name': vol_name,
                    'pathType': "File",
                    'readOnly': readOnly,
                })
                file_spec: config_util.YamlDict = {
                    'path': host_path,
                }
                if file_content is not None:
                    file_spec['content'] = file_content
                files.append(file_spec)
                i += 1
        feature_gates = controlplane.get('featureGates', None)
        if isinstance(feature_gates, Dict):
            clusterConfiguration['featureGates'] = feature_gates
        localEndpoint = controlplane.get('localEndPoint', None)
        if isinstance(localEndpoint, dict):
            host = localEndpoint.get('host', None)
            port = localEndpoint.get('port', 6443)
            if isinstance(host, str) and isinstance(port, int):
                initConfiguration['localAPIEndpoint'] = {
                    'advertiseAddress': host,
                    'bindPort': port
                }
    return spec


def kubeadm_controlplane_manifest(
    cluster: updater.Cluster, version: str
) -> config_util.YamlDict:
    cp_spec = cluster.controlplane
    # inherited during merge from customer spec
    replicas = cast(int, cp_spec.get('replicas', 1))
    server_config = cast(config_util.YamlDict, cp_spec.get('serverConfig', {}))
    config = enhance_config(cluster, server_config)
    cluster.vault.inject('cp', config)
    image = machines.image_path(cluster, config, version)
    validator.validate_node(
        cluster.error_handler, image, config, cluster.verify)
    baremetal = cast(
        config_util.YamlDict, cp_spec.get('hostSelector', {}))
    uid = updater.build_uid(f'{image}{yaml.dump(baremetal, sort_keys=True)}')
    key = f'controlplane-{cluster.cluster_name}'
    extra_args = cast(Dict[str, str], cp_spec.get('kubeletExtraArgs', {}))
    taints = cast(List[config_util.YamlObj], cp_spec.get('taints', []))
    pool = cp_spec.get('pool', None)
    if pool is not None:
        labels = {'kanod.io/baremetalpool': pool}
    else:
        labels = None
    spec: config_util.YamlDict = {
        'replicas': replicas,
        'version': version,
        'machineTemplate': {
            'infrastructureRef': {
                'kind': 'Metal3MachineTemplate',
                'apiVersion': updater.CAPM3_GROUP,
                'name': f'controlplane-{cluster.cluster_name}-{uid}'},
        },
        'kubeadmConfigSpec': kubeadm_config_spec(
            cluster, config, cluster.k8sconfig, key, extra_args, taints)
    }
    manifest: config_util.YamlDict = {
        'kind': 'KubeadmControlPlane',
        'apiVersion': updater.CP_GROUP,
        'metadata': cluster.metadata(
            f'controlplane-{cluster.cluster_name}', labels=labels),
        'spec': spec
    }
    return manifest


def kubeadmconfig_manifest(
    cluster: updater.Cluster, md_spec: config_util.YamlDict,
    version: str
) -> config_util.YamlDict:
    name = cast(str, md_spec.get('name', ''))
    key = f'md-{cluster.cluster_name}-{name}'
    config = cast(config_util.YamlDict, md_spec.get('serverConfig', {}))
    config = enhance_config(cluster, config)
    cluster.vault.inject(f'md-{name}', config)
    image = machines.image_path(cluster, config, version)
    validator.validate_node(
        cluster.error_handler, image, config, cluster.verify)
    extra_args = cast(Dict[str, str], md_spec.get('kubeletExtraArgs', {}))
    taints = cast(List[config_util.YamlObj], md_spec.get('taints', []))
    specTemplate = kubeadm_config_spec(
        cluster, config, None, key, extra_args, taints)
    spec: config_util.YamlDict = {'template': {'spec': specTemplate}}
    manifest: config_util.YamlDict = {
        'kind': 'KubeadmConfigTemplate',
        'apiVersion': updater.BOOTSTRAP_GROUP,
        'metadata': cluster.metadata(f'md-{cluster.cluster_name}-{name}'),
        'spec': spec
    }
    return manifest
